#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

bool nickconvert (char temp[]) {
    char *at = strstr(temp, " says:");
    if (at) {
        //memmove (temp + 1, temp, at - temp);
        temp[0] = '<';
        at[0] = '>';
        at[1] = '\0';
        return true;
    } else
        return false;
}

void convert (istream &in, ostream &out) {
    char line[1000], temp[1000], *ptr;
    //int brace = 0;
    bool brace = false, slashstr = false, blank = true, nonew = false;
    bool nickfound = false;
    cout<<"convert";
    while (!in.eof()) {
        //cout<<"aline";
        in.getline(line, 1000);
        slashstr = false;
        blank = true;
        nonew = false;
        ptr = temp;
        
        for (int i = 0; line[i]; ++i) {
            if (line[i] == '\\' && line[i + 1] != '\\')
                slashstr = true;
            if (line[i] == '{')
                //++brace;
                brace = true;
            if (line[i] == ' ')
                slashstr = false;
                
            if (!slashstr && !brace) {
                //out << line[i];
                *ptr++ = line[i];
                blank = false;
            }
                
            if (line[i] == '}')
                //--brace;
                brace = false;
        }
        *ptr = '\0';
        if (nickfound && blank)
            out << endl;
        nickfound = nickconvert(temp);
        if (!blank && !nonew && !nickfound) {
            out << temp;
            out << endl;
        } else
            out << temp;
    }
}


int main(int argc, char *argv[])
{
    int i = 1;
    cout << argc << endl;
    while (i < argc) {
        cout << "i=" << i << endl;
        cout << argv[i] << endl;
        //ifstream in("test.rtf");
        //ofstream out("test.txt");
        ifstream in(argv[i]);
        strcpy(strstr(argv[i], ".rtf"), ".txt");
        cout << argv[i] << endl;
        ofstream out(argv[i]);
        convert (in, out);
        ++i;
    }

}
