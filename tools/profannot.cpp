// Annotate a source file with line-by-line ms values from
// gprof -l


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>

using namespace std;

int main (int argc, char **argv) {

  int lines =  4000;
  int *linew = (int*)calloc(sizeof(int), lines);

  //  for (int i = 0; i < lines; i++)

  if (argc < 3) {
    printf("Annotate a sourcefile\n");
    printf("profannot prof_l.txt source.x\n");
	return 1;
  }

  ifstream proff(argv[1]), srcf(argv[2]);

  char buf[1000];
  bool tablestarts = false;

  while (!proff.eof()) {
    proff.getline(buf, 999);

    if (tablestarts) {
      char *temp = strstr(buf, argv[2]);
      if (temp) {
        int lineno = atoi(temp + strlen(argv[2]) + 1);
        int timeno = atoi(buf + 23);
	if (timeno == 0)
	  break;
        linew[lineno] = timeno;
      }
    } else {
      if (strstr(buf, " time"))
	tablestarts = true;
    }
  }
  /*
    for (int i=0; i < 2000; i += 10) {
    printf("%d %d %d %d %d %d %d %d %d %d\n", linew[i], linew[i + 1], linew[i + 2], linew[i + 3], linew[i + 4], linew[i + 5], linew[i + 6], linew[i + 7],linew[i + 8], linew[i + 9]);
    }
  */
  strcpy(buf, argv[2]);
  strcat(buf, "2.txt");
  ofstream out(buf);

  int i = 1;
  while (!srcf.eof()) {

    sprintf(buf, " %2d |", linew[i]);

    srcf.getline(buf + strlen(buf), 999);

    out.write(buf, strlen(buf));
    out.put('\n');

    i++;
  }


  return 0;
}
