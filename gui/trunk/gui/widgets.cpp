#include <SDL/SDL.h>
#include "widgets.h"
#include "wm.h"
//#include <SDL/SDL_gfxPrimitives.h>

using namespace std;

// no checks done!!
void Widget::layout(Window *parent) {
    if (spacel >= 0 && spacer >= 0)
        w = parent->frame.x2 - parent->frame.x1 - spacel - spacer;
    if (spacet >= 0 && spaceb >= 0)
        h = parent->frame.y2 - parent->frame.y1 - spacet - spaceb;
        
    if (spacel >= 0)
        x = parent->frame.x1 + spacel;
    else if (spacer >= 0)
        x = parent->frame.x2 - spacer;
    else
        x = parent->frame.x1;
        
    if (spacet >= 0)
        y = parent->frame.y1 + spacet;
    else if (spaceb >= 0)
        x = parent->frame.y2 - spaceb;
    else
        y = parent->frame.y1;
    
    parent->frame.y1 = y + h;
}

void BorderWidget::draw(SDL_Surface *surface) {
    SDL_Rect tab;
    drawrect(surface, 0, 0, surface->w - 1, surface->h - 1, colour1);
    tab.x = tab.y = 1;
    tab.w = surface->w - 2;
    tab.h = tabsize;
    SDL_FillRect(surface, &tab, colour2);
    drawrect(surface, 1, 1, surface->w - 2, surface->h - 2, colour2);
    drawrect(surface, 2, tabsize + 1, surface->w - 3, surface->h - 3, colour1);
}

void BorderWidget::layout(Window *parent) {
    /*parent->linex =*/ parent->frame.x1 += 4;
    /*parent->liney =*/ parent->frame.y1 += 3 + tabsize;
    parent->frame.x2 -= 4;
    parent->frame.y2 -= 4;
}

void TextWidget::draw(SDL_Surface *surface) {
    SDL_Rect destrect;
    destrect.x = x;
    destrect.y = y;
    SDL_BlitSurface(textf->getSurface(), NULL, surface, &destrect);
}

void TextWidget::layout(Window *parent) {
    destroy();
    x = parent->frame.x1;
    y = parent->frame.y1;
    textf = new CTextField(str, parent->frame.x2 - parent->frame.x1, parent->frame.y2 - parent->frame.y1);
    
    parent->frame.y1 += textf->getHeight();
}

/*
Widget::placement_t BorderWidget::placementType() {
    w = 0;
    h = 14;
    return Widget::HEADER;
}
*/

Button::Button() {
    indented = false;
    normalc = 0xcbc8d5;
    indentc = 0xaaa3bd;
    borderc1 = 0xd5d3db;
    borderc2 = 0xa29db0;
    w = 50;
    h = 14;
    takesclicks = true;
}

void Button::pressed() {
    cout << "button pressed!" << endl;
    //parent->signalUpdate();
    parent->sendAlert(this, 0);
}

void Button::draw(SDL_Surface *surface) {
    cout << "drawing button with indent = " << indented << endl;
    SDL_Rect destrect;
    destrect.x = x + 1;
    destrect.y = y;
    destrect.w = w - 2;
    destrect.h = 1;
    SDL_FillRect(surface, &destrect, indented ? borderc2 : borderc1);
    destrect.y = y + h - 1;
    SDL_FillRect(surface, &destrect, indented ? borderc1 : borderc2);
    destrect.x = x;
    destrect.y = y + 1;
    destrect.w = 1;
    destrect.h = h - 2;
    SDL_FillRect(surface, &destrect, indented ? borderc2 : borderc1);
    destrect.x = x + w - 1;
    destrect.h = h - 1;
    SDL_FillRect(surface, &destrect, indented ? borderc1 : borderc2);
    
    destrect.x = x + 1;
    destrect.y = y + 1;
    destrect.w = w - 2;
    destrect.h = h - 2;
    SDL_FillRect(surface, &destrect, indented ? indentc : normalc);
}

bool Button::leftPress(int x, int y) {
    parent->needsredraw = true;
    if (inRect(x - parent->position.x, y - parent->position.y, RectBlock(x, y, w, h))) {
        indented = true;
        return false;
    } else {
        indented = false;
        return true;
    }
        
}

bool Button::leftRelease(int x, int y) {
    indented = false;
    parent->needsredraw = true;
    pressed();
}

bool Button::leftDrag(int x, int y) {
    if (inRect(x - parent->position.x, y - parent->position.y, RectBlock(x, y, w, h))) {
        indented = true;
        return false;
    } else {
        parent->needsredraw = true;
        indented = false;
        return true;
    }
}

bool Button::rightRelease(int x, int y) {

}

void drawline(SDL_Surface *surface, int x1, int y1, int x2, int y2, int c) {
    int *ptr, nextline, i, j;
    float grad = ((float)(y2 - y1)) / (x2 - x1 + 1), accul = 0.0f;
    
    ptr = (int *)surface->pixels + x1 + y1 * (surface->pitch / 4);
    nextline = (surface->pitch / 4) - x2 + x1;

    //cout << "x1 = " << x1 << " x2 " << x2 << endl;
    
    accul = y1;
    int start, finish, step;
    for (i = x1; i <= x2; i++) {
        start = (int)accul;
        finish = (int)(accul += grad);
        step = finish > start ? 1 : -1;
        finish += step;
        //cout << "start = " << start << " end = " << finish << endl;
        for (j = start; j != finish; j += step) {
            *((int *)surface->pixels + i + j * (surface->pitch / 4)) = c;
        }
    }
    //cout << "accul = " << accul << " y2 = " << y2 << endl;

}


void Graph::draw(SDL_Surface *surface) {
    int i, c = 0xffff00;
    
    vector<GraphingData>::iterator it;
    cout << "graphis size =  " << graphs.size() << endl;
    for ( it = graphs.begin(); it != graphs.end(); it++) {
    
        //cout << "numpoints = " << it->numpoints << "xscale = " << it->xscale << "yscale = "<< it->yscale<< endl;
    
        for (i = 0; i < it->numpoints - 1; i++) {
        
    //    lineRGBA(surface, x + (int)(xscale * i), y + h - (int)(data[i] * yscale),
	//		      (int)(xscale * (i + 1)), y + h - (int)(data[i + 1] * yscale), 0xffffff, 0xffffff, 0, 0);

            drawline(surface, x + (int)(it->xscale * i), y + h - (int)(it->data[i] * it->yscale),
			      x + (int)(it->xscale * (i + 1)), y + h - (int)(it->data[i + 1] * it->yscale), c);
			

			      
        }
        c <<= 8;
        c += 0xf;
    }
        
    
}

void Graph::layout(Window *parent) {
    Widget::layout(parent);
    vector<GraphingData>::iterator it;
    for ( it = graphs.begin(); it < graphs.end(); it++) {
        it->xscale = (float)w/it->numpoints;
        int ymax = 0;
    
        for (int i = 0; i < it->numpoints; i ++) {
            if (it->data[i] > ymax)
                ymax = it->data[i];
        }
        it->yscale = (float)h/ymax;
    }
}

void drawrect(SDL_Surface *surface, int x1, int y1, int x2, int y2, int c) {
    int *ptr, nextline, othercol, i;
    
    ptr = (int *)surface->pixels + x1 + y1 * (surface->pitch / 4);
    nextline = (surface->pitch / 4) - x2 + x1;
    othercol = x2 - x1;
    
    for (i = x1; i <= x2; i++)
        *ptr++ = c;
        
    ptr += nextline - 1;
    
    for (i = y1 + 1; i < y2; i++) {
        *ptr = c;
        ptr += othercol;
        *ptr = c;
        ptr += nextline;
    }
    
    for (i = x1; i <= x2; i++)
        *ptr++ = c;
}

SDL_Surface *createrect(int w, int h, Uint8 r, Uint8 g, Uint8 b) {
    const SDL_VideoInfo *vinfo = SDL_GetVideoInfo();
//    int colour = SDL_MapRGB(vinfo->vfmt, r, g, b);
/*    SDL_Surface *surface = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, vinfo->vfmt->BitsPerPixel, 
                                  vinfo->vfmt->Rmask, vinfo->vfmt->Gmask, vinfo->vfmt->Bmask, vinfo->vfmt->Amask); */
                                  
    SDL_Surface *surface = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 32, 
                                  0xff, 0xff00, 0xff0000, 0);
    int colour = SDL_MapRGB(surface->format, r, g, b);
    if (!surface) {
        cerr << "createrect: Failed to create surface" << endl;
        exit(1);
    }
    SDL_FillRect(surface, NULL, colour);
    return surface;
}
