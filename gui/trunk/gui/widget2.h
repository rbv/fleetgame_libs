class Widget {
    friend class WM;

    protected:
        int id;
        RectBlock position;
        
        void place(int x, int y) {
            position.x = x;
            position.y = y;
        } 
        
        /* A widget is in one of 3 drawing modes:
           -It's parent widget passes it an SDL_Surface, onto which
            it draws itself and its children
           -It rendered itself and children once to private surface to
            save drawing time next time it redraws
           -It draws itself to an internal surface which it passes up
            to be drawn directly to the screen (eg. toplevel widgets and
            video frames)
         */
         
        enum { DRAW_NORMAL, DRAW_CACHE, DRAW_SPRITE } drawmode;
        
        //draw!
        // surface is null if a toplevel widget (all toplevels should be type 3)
        // this calls redraw() on self and children when necessary
        void draw(SDL_Surface *surface);
        
        // this is where the difference in draw types occurs
        void redraw(SDL_Surface *surface, Widget *spritemaster);
        
        // actual, widget-specific drawing method
        virtual void render(SDL_Surface *surface);
        
        virtual void layout(Window *parent);
 
        //type 2:
        SDL_Surface *rendersurface
        //type 3:
        CSprite *rendersprite;

        
        //parent widget (NULL for toplevel)
        Widget *parent;
        
        vector<Widget *> widgetlist;
        
        void addWidget(Widget &widget);
        
        //type 3 wigets only:
        //each type 3 widget has a linked list of type 3 widgets which need to
        //be drawn with it
        Widget *drawnext;
        Widget *drawchild, *drawlast;
        
        // sprite needs to be rerendered
        bool needsredraw;
        // wants attention from WM (call to update())
        bool needsattention;
        
        // pass need redraw flag up widget tree
        void setNeedRedraw() {
            if (!needsredraw) {
                needsredraw = true;
                if (parent)
                    parent->setNeedRedraw();
            }
        }
        /*************/
        
        
        // input code
        
        bool takesclicks,  // consider for clicks (may pass down to subwidgets even if doesn't take)
             grabsclicks,   // grabs click events (clickable object)
             takesfocus,    // focus when clicked
             hasfocus,      // focus for text input/other
             takesdrags;    // can be dragged
             //movestotop,    // for toplevel widgets, moves widnow to top
 
             
        // whether a point is inside the widget, coordinates
        bool pointInside(int x, int y);
        
        // mouse events
        
        // mouse was depressed on widget, but moved away
        // called if takesdrags false
        virtual void lostClickDown() {}
        virtual void lostFocus() { hasfocus = false; }
        virtual void leftPress(int x, int y) { }
        virtual void leftRelease(int x, int y) {}
        virtual void leftDrag(int ix, int iy, int fx, int fy) {} // takesdrags true
        virtual void rightRelease(int x, int y) {}
        
        // time in ms
        virtual void mouseOver(int x, int y) {}
        
        // set by WM
        int mouseovertime;
        
        // return true to pass the click on to a subwidget, false to block
 //       virtual bool passClick(Widget *widget) { return true; }
        
        
        virtual Widget *findClickedWidget(int x, int y);
        
};


class OTHERWIDGETSTUFF {
        
        
        /*****************//////////////*********************/
        
        CMouseInputHandler *clickableWidget(int x, int y);
        
        CKeyInputHandler *activekeyhandler;
        
        // Widget recieving messages from the current mouse callgroup
        // TODO: clean this up!
        CMouseInputHandler *activemousehandler;
        
        // Pass handling onto some other widget
        
        // this is stuff for positioning added widgets
        /*
        int linex, liney;
        int nextlinex, nextliney;
        int nextcolx, headerh;
        int limleft, limright, limtop, limbottom;  // margins
        */
        RectPoints frame;
        int nextcolx;      //unused
     //   int linex, liney;
        
        //void positionWidget(Widget *widget);
    protected:
        // Backdrop settings: w and h from position
        // Colour of background
        int backr, backg, backb;
        
        CMouseInputHandler *windowClickHook;
        
    public:      
        Window();
        Window(int w, int h, Uint8 r, Uint8 g, Uint8 b);
        ~Window();
        
        void setClickHook(CMouseInputHandler *hook) { windowClickHook = hook; }
        
        /* Input relegation */
        virtual bool addChar(char);
        
        // NOTE: all functions but leftDrag throw away messages with coords out-
        // side window, even though leftRelease is sent to the current callgroup
        virtual bool leftPress(int x, int y);
        virtual bool leftRelease(int x, int y);
        virtual bool leftDrag(int x, int y);
        virtual bool rightRelease(int x, int y);
        

        ////////////
    protected:
        
        //the following are NULL if don't take input
        CKeyInputHandler *texthandler;
        CMouseInputHandler *mousehandler;
        
        // Function to be called once per tick whenever needsattention
        virtual void update() {}
        

        
    public:
        WindowPane() : handle(NULL), texthandler(NULL), mousehandler(NULL), needsredraw(true), needsattention(false) {}
        ~WindowPane() {
            if (handle != NULL)
                WM::removeWindow(this);
        }

        bool isActive() {
            return this == WM::topWindow();
        }
        
       
        ///////////

        void loadSprite(SDL_Surface *surface) {
            position.w = surface->w;
            position.h = surface->h;
            if (!sprite)
                sprite = new CSprite;
            sprite->load(surface);
        }
        
        string name;  // for debugging purposes
        
    protected:
        //bool needupdate;
        
        bool takesclicks, takeskeys, takeskeyfocus;  // takeskeyfocus = selectable
        
        int x, y;
        // size of the space the widget takes up, NOT sprite size!
        int w, h;
        // where to start sameline widgets relative to x, y (possibly inside w, h above)
        int endlx, endly;
        // these are used by the default layout: pixels to each side
        // of the current space, -1 indicates no attachment
        int spacel, spacer, spacet, spaceb;
        


        
    public:
        Widget() : takesclicks(false), takeskeys(false), takeskeyfocus(false)
                   , spacel(-1), spacer(-1), spacet(-1), spaceb(-1) {}
        
        enum placement_t { SAMELINE, NEWLINE, NEWCOL, HEADER };
        //virtual placement_t placementType() = 0;
        
        Widget &stretchX() {
            spacel = spacer = 0;
            return *this;
        }
        Widget &stretchY() {
            spacet = spaceb = 0;
            return *this;
        }
        Widget &leftMargin(int x) {
            spacel = x;
            return *this;
        }
        Widget &rightMargin(int x) {
            spacer = x;
            return *this;
        }
        Widget &topMargin(int x) {
            spacet = x;
            return *this;
        }
        Widget &bottomMargin(int x) {
            spaceb = x;
            return *this;
        }
    
};
