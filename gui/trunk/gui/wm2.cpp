#include "wm.h"

static list<WindowPane *>windowList;
static int windowidctr = 0;

void WM::debug() {
    cout << "windowlist contains " << windowList.size() << endl;

}

void WM::draw() {
    
    
    
    
    
    int redrawthis, redrawback = 0;
    for (windowh it = windowList.begin(); it != windowList.end(); ++it) {
        //cout << "drawing window " << (*it)->name << endl;
        redrawthis = 0;
        RectBlock *pos = &(*it)->position, *lpos = &(*it)->lastpos;
        if ((*it)->needsredraw) {
            //cout << "window " << (*it)->id << " needs update" << endl;
            (*it)->redraw();
            redrawthis = 1;
        }
        /* feature left for another time
        if (pos->x != lpos->x || pos->y != lpos->y || pos->w != lpos->w || pos->h != lpos->h) {
            redrawback = 1;
            *lpos = *pos;
        }
            
        if (redrawthis | redrawback) */
            (*it)->sprite.draw(*pos);
    }
    //cout << "drawing window sprite " << &(*it)->sprite << " at x " << (*it)->position.x << " y " << (*it)->position.y << endl;
}

void WM::doUpdates() {
    for (windowh it = windowList.begin(); it != windowList.end(); ++it)
        if ((*it)->needsattention) {
            (*it)->needsattention = false;
            (*it)->update();
/*          if ((*it)->updatefunc)
                ((*it)->*(*it)->updatefunc)();
            else
                cout << "Warning: attention bit without update method" << endl;
*/
        }
}
    

void WM::addWindow(WindowPane *window) {
    if (window->handle == NULL) {
        window->handle = windowList.insert(windowList.end(), window);
        attachHooks(window);
        window->id = windowidctr++;
    }
}

void WM::removeWindow(WindowPane *window) {
    windowList.erase(window->handle);
    window->handle = NULL;
    removeHooks(window);
//    if (!windowList.empty())
//        attachHooks(windowList.back());
}

void WM::moveToTop(WindowPane *window) {
    if (!windowList.empty() && (window->handle != NULL)) {        
        //windowList.push_back(*wh);
        //windowList.erase(wh);
        
        //removeHooks(windowList.back());
        windowList.splice(windowList.end(), windowList, window->handle);
        removeHooks(window);  // add to front of queue
        attachHooks(window);
    } else {
        cerr << "Tried to move a window to the top of an empty window stack!" << endl;
        exit(-1);
    }
}

WindowPane *WM::topWindow() {
    return windowList.back();
}

bool leftPress(int x, int y) {
    for (windowh it = windowList.begin(); it != windowList.end(); ++it) {
        
        if ((*it)->w->pointInside(x, y)) {
            Widget *clicked = (*it)->w->findClickedWidget(x, y);
            if (clicked) {
                if ((*it)->movestotop) {
                    moveToTop(*it);
                }
                if (clicked->takesfocus) {
                    Widget &*foc = (*it)->focused;
                    if (foc != clicked) {
                        if (foc)
                            (foc)->lostFocus();
                        foc = clicked;
                    }
                    clicked->hasfocus = true;
                }                    
                clicked->leftPress(x, y);
                
   
/*

            Widget *clicked = (*it)->w;
            while (clicked->takesclicks) {
                vector<Widget *>::iterator wid;
                for (wid = clicked->widgetlist.begin(); clicked->widgetlist.begin(); ++wid) {
                    if ((*wid)->pointInside(x, y)) {
                        
                        
*/
}


Widget *Widget::findClickedWidget(int x, int y) {
    vector<Widget *>::iterator wid;
    for (wid = widgetlist.begin(); widgetlist.begin(); ++wid) {
        if ((*wid)->takesclicks && (*wid)->pointInside(x, y)) {
            if (Widget *result = (*wid)->findClickedWidget(x, y))
                return result;
        }
    }
    if (grabsclicks)
        return this;
    return NULL;
}

bool leftRelease(int x, int y)
bool leftDrag(int ix, int iy, int fx, int fy) {
    
    
bool rightRelease(int x, int y);

void WM::removeHooks(WindowPane *window) {
    if (window->texthandler)
        input->removeHandler(window->texthandler);
    if (window->mousehandler)
        input->removeHandler(window->mousehandler);
}

void WM::attachHooks(WindowPane *window) {
    if (window->texthandler)
        input->addHandler(window->texthandler, true);
    if (window->mousehandler)
        input->addHandler(window->mousehandler, true);
}

Window::Window() {
    backr = 195;
    backg = 195;
    backb = 210;
    activekeyhandler = NULL;
    activemousehandler = NULL;
    windowClickHook = NULL;
    texthandler = this;
    mousehandler = this;
}

Window::Window(int w, int h, Uint8 r, Uint8 g, Uint8 b) {
    activekeyhandler = NULL;
    activemousehandler = NULL;
    windowClickHook = NULL;
    loadSprite(createrect(w, h, r, g, b));
    texthandler = this;
    mousehandler = this;
}

Window::~Window() {
    // widgets are assumed to be destroyed elsewhere
    // (in the virtual destructors of inheriting classes)
}

void Window::addWidget(Widget &widget) {
    widgetlist.push_back(&widget);
    widget.parent = this;
    needsredraw = true;   
}

/*
void Window::positionWidget(Widget *widget) {
    // placementType has the opportunity to calculate the fields below
    switch(widget->placementType()) {
        case Widget::SAMELINE:
            widget->x = linex;
            widget->y = liney;
            if (widget->y + widget->h > nextliney)
                nextliney = widget->y + widget->h;
            goto shifting;
        case Widget::NEWLINE:
            linex = widget->x = nextlinex;
            liney = widget->y = nextliney;
            nextliney += widget->h;
        shifting:
            linex += widget->endlx;
            liney += widget->endly;
            if (widget->x + widget->w > nextcolx)
                nextcolx = widget->x + widget->w;
            break;
        case Widget::NEWCOL:
            widget->x = nextcolx;
            liney = nextliney = widget->y = headerh;
            linex = nextlinex = nextcolx += widget->w;
            break;
        case Widget::HEADER:
            linex = nextlinex = nextcolx = widget->x = 0;
            widget->y = headerh;
            liney = nextliney = headerh += widget->h;
    }
}
*/

void Window::redraw() {
    cout << "Window::redraw(): colour r"<< backr << " g" << backg << " b" << backb << endl;
    SDL_Surface *tempsurface;
    vector<Widget *>::iterator it;
    
    //linex = liney = 0;
    //nextlinex = nextliney = 0;
    //nextcolx = headerh = 0;
    
    frame.x1 = frame.y1 = 0;
    frame.x2 = position.w;
    frame.y2 = position.h;
    nextcolx = 0;
    
    tempsurface = createrect(position.w, position.h, backr, backg, backb);
    cout << "tempsur = " << tempsurface << endl;
    
    for (it = widgetlist.begin(); it < widgetlist.end(); it++) {
        cout << "adding widget" << endl;
        //positionWidget(*it);
        (*it)->layout(this);
        (*it)->draw(tempsurface);
        //(*it)->draw(this);
        //(*it)->needupdate = false;
    }
        
    loadSprite(tempsurface);
    needsredraw = false;
}

CMouseInputHandler *Window::clickableWidget(int x, int y) {
    vector<Widget *>::iterator it;
    
    for (it = widgetlist.begin(); it < widgetlist.end(); it++) {
        if ((*it)->takesclicks || (*it)->takeskeyfocus) {
            if (inRect(x, y, RectBlock((*it)->x + position.x, (*it)->y + position.y, (*it)->w, (*it)->h))) {
                if ((*it)->takeskeyfocus)
                    activekeyhandler = (CKeyInputHandler *)*it;
                return dynamic_cast<CMouseInputHandler *>(*it);
            }
        }
    }
    if (activekeyhandler && ((Widget *)activekeyhandler)->takeskeyfocus)
        activekeyhandler = NULL;
    
    return NULL;
}

bool Window::addChar(char ch) {
    if (activekeyhandler)
        activekeyhandler->addChar(ch);
}

bool Window::leftPress(int x, int y) {
    if (!inRect(x, y, position))
        return true;
    CMouseInputHandler *handler;
    handler = clickableWidget(x, y);
    if (handler) {
        handler->leftPress(x, y);
        activemousehandler = handler;
    }
    else if (windowClickHook)
        return windowClickHook->leftPress(x, y);
    return false;
}

bool Window::leftRelease(int x, int y) {
    if (!inRect(x, y, position))
        return true;
    CMouseInputHandler *handler;
    if (activemousehandler) {
        cout << "c\n";
        if (!activemousehandler->leftRelease(x, y))   // return true to drop interest in this click
            return false;
    }
    handler = clickableWidget(x, y);
    activemousehandler = NULL;
    if (handler) {
        cout << "a\n";
        handler->leftRelease(x, y);
        cout << "b";
    }
    else if (windowClickHook)
        return windowClickHook->leftRelease(x, y);
    return false;
}

bool Window::leftDrag(int x, int y) {
    /*if (!inRect(x, y, position))
        return true;*/
    CMouseInputHandler *handler;
    if (activemousehandler)
        if (!activemousehandler->leftDrag(x, y))   // return true to drop interest in this click
            return false;
    handler = clickableWidget(x, y);
    activemousehandler = NULL;
    if (handler) {
        handler->leftDrag(x, y);
        activemousehandler = handler;
    }
    else if (windowClickHook)
        return windowClickHook->leftDrag(x, y);
    return false;
}

bool Window::rightRelease(int x, int y) {
    if (!inRect(x, y, position))
        return true;
    CMouseInputHandler *handler;
    handler = clickableWidget(x, y);
    if (handler)
        handler->rightRelease(x, y);
    else if (windowClickHook)
        return windowClickHook->rightRelease(x, y);
    return false;
}


MoveableWindowControl::MoveableWindowControl(Window *_parent) {
    limleft = 0;
    limtop = 0;
    limright = video->getWidth();
    limbottom = video->getHeight();
    parent = _parent;
    parent->setClickHook(this);
}

/*
bool MoveableWindowControl::inWindow(int x, int y) {
    if (x >= position.x && x < position.x + position.w && y >= position.y && y < position.y + position.h)
        return true;
    return false;
}
*/

bool MoveableWindowControl::leftPress(int x, int y) {
    cout << "window" << parent->id << " recieved click\n";
    /*if (!inWindow(x, y)) {
        clickoffx = -1;
        cout << "not in window\n";
        return true;
    }*/
    clickoffx = x - parent->position.x;
    clickoffy = y - parent->position.y;
    return false;
}

bool MoveableWindowControl::leftRelease(int x, int y) {
    //return !inWindow(x, y);
    clickoffx = -1;
    return false;
}

bool MoveableWindowControl::leftDrag(int x, int y) {
    if (clickoffx != -1) {
        int &px = parent->position.x, &py = parent->position.y;
        int &pw = parent->position.w, &ph = parent->position.h;
        px = x - clickoffx;
        py = y - clickoffy;
        px = px < limleft ? limleft : px;
        px = px + pw >= limright ? limright - pw : px;
        py = py < limtop ? limtop : py;
        py = py + ph >= limbottom ? limbottom - ph : py;
        return false;
    }
    return true;
}
        
bool MoveableWindowControl::rightRelease(int x, int y) {
    //return !inWindow(x, y);
    return false;
}
