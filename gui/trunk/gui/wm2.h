#ifndef WM_H
#define WM_H

#include <list>
#include <vector>
#include <io/video2.h>
#include <io/input.h>
#include "widgets.h"
#include <io/rect.h>

class WindowPane;

class WM : public CInputDispatcher<CMouseInputHandler>, CMouseInputHandler {
    private:
        // add and remove a window's hooks from CInput
        static void removeHooks(WindowPane *window);
        static void attachHooks(WindowPane *window);
        
        virtual bool leftPress(int x, int y);
        virtual bool leftRelease(int x, int y);
        virtual bool leftDrag(int x, int y);
        virtual bool rightRelease(int x, int y);
        

    public:
        /* new philosophy: only the Window should hang onto
         * windowh handles, and all other code should deal with
         * Window pointers instead.
         */

        typedef list<WindowPane *>::iterator windowh;
        
        static void addWindow(WindowPane *window);
        static void removeWindow(WindowPane *window);
        static void moveToTop(WindowPane *window);
        static WindowPane *topWindow();
        
        static void draw();
        
        // furfill attention requests by windows, call this before draw()
        static void doUpdates();
        
        static void debug();
        

};

/* Wrapper class for Widget - decide later whether useless */
class WindowPane {
    friend class WM;
    private:
        // nobody else needs this, used by WM
        WM::windowh handle;
        RectBlock lastpos;
        
        bool movestotop;
        
        Widget *w;
        
        // inactive focus is widget to focus on window becoming active
        Widget *focused, *inactivefocus;
        
}

/* A window pane - minimal window class */
class WindowPane {
    friend class WM;
    private:
        // nobody else needs this, used by WM
        WM::windowh handle;
        RectBlock lastpos;
        
    protected:
        CSprite sprite;
        //Window might contain more sprites here- dynamic items
        
        // sprite needs to be rerendered
        bool needsredraw;
        // wants attention from WM (call to update())
        bool needsattention;
        
        //the following are NULL if don't take input
        CKeyInputHandler *texthandler;
        CMouseInputHandler *mousehandler;
        
        // Function to be called once per tick whenever needsattention
        virtual void update() {}
        
        // Function to be called when sent a message
        // the pointer should be pointer to the object raising 
        virtual void sendAlert(void *sender, int arg) {}
        
        // Called by WM when needsredraw is set
        virtual void redraw() { needsredraw = false; cout << "null window drawer\n"; }
        
    public:
        WindowPane() : handle(NULL), texthandler(NULL), mousehandler(NULL), needsredraw(true), needsattention(false) {}
        ~WindowPane() {
            if (handle != NULL)
                WM::removeWindow(this);
        }
        
        int id;
        
        RectBlock position;
        
        void signalUpdate() {
            needsattention = true;
        }

        bool isActive() {
            return this == WM::topWindow();
        }
        
        void place(int x, int y) {
            position.x = x;
            position.y = y;
        }
        
        void loadSprite(SDL_Surface *surface) {
            position.w = surface->w;
            position.h = surface->h;
            sprite.load(surface);
            needsredraw = false;
            cout << "redraw bit wiped in loadSprite\n";
        }
        
        string name;  // for debugging purposes
};

class Window : public WindowPane, public CMouseInputHandler, public CKeyInputHandler {
    friend class Widget;
    friend class BorderWidget;
    friend class TextWidget;
    friend class Button;
    private:
        vector<Widget *> widgetlist;
        
        CMouseInputHandler *clickableWidget(int x, int y);
        
        CKeyInputHandler *activekeyhandler;
        
        // Widget recieving messages from the current mouse callgroup
        // TODO: clean this up!
        CMouseInputHandler *activemousehandler;
        
        // Pass handling onto some other widget
        
        // this is stuff for positioning added widgets
        /*
        int linex, liney;
        int nextlinex, nextliney;
        int nextcolx, headerh;
        int limleft, limright, limtop, limbottom;  // margins
        */
        RectPoints frame;
        int nextcolx;      //unused
     //   int linex, liney;
        
        //void positionWidget(Widget *widget);
    protected:
        // Backdrop settings: w and h from position
        // Colour of background
        int backr, backg, backb;
        
        //enum widgetplace {};
        void addWidget(Widget &widget);
        
        // derived classes should probably not reimplement this
        virtual void redraw();
        
        CMouseInputHandler *windowClickHook;
        
    public:      
        Window();
        Window(int w, int h, Uint8 r, Uint8 g, Uint8 b);
        ~Window();
        
        void setClickHook(CMouseInputHandler *hook) { windowClickHook = hook; }
        
        /* Input relegation */
        virtual bool addChar(char);
        
        // NOTE: all functions but leftDrag throw away messages with coords out-
        // side window, even though leftRelease is sent to the current callgroup
        virtual bool leftPress(int x, int y);
        virtual bool leftRelease(int x, int y);
        virtual bool leftDrag(int x, int y);
        virtual bool rightRelease(int x, int y);
};

class MoveableWindowControl : public CMouseInputHandler {
    private:
        Window *parent;
        int clickoffx, clickoffy;
        int limleft, limright, limtop, limbottom;
        
        //bool inWindow(int x, int y);
    public:
        MoveableWindowControl(Window *_parent);
        
        // CMouseInputHandler members
        virtual bool leftPress(int x, int y);
        virtual bool leftRelease(int x, int y);
        virtual bool leftDrag(int x, int y);
        virtual bool rightRelease(int x, int y);
};


/*   Layout:
 *
 * widget layout has two aspects: the way in which a widget arranges its children
 * and the way in which a widget sizes itself in that layout
 */

class WidgetLayoutScheme {
    private:
        // minimum width and height, if calculated
        int minw, minh;
    public:
        void defaultLayout(Widget *widget) {}
        // call when all widgets have been added to correctly position
        void finalise() {}
    
    
};

// Widgets arranged in wrapping lines: either appedn to the current line, or start a new one.
class LineLayout : public WidgetLayoutScheme {
    void defaultLayout(Widget *widget) { newLine(widget); }
    
    void newLine(Widget *widget);
    void appendLine(Widget *widget);
};

class GridLayout : public WidgetLayoutScheme {
    
};

#endif
