#ifndef WIDGETS_H
#define WIDGETS_H

//#include "wm.h"
#include <io/video2.h>
#include <io/text.h>
#include <io/input.h>

#include <vector>

class Widget {
    friend class Window;
    private:
        
    protected:
        //bool needupdate;
        
        bool takesclicks, takeskeys, takeskeyfocus;
        
        int x, y;
        // size of the space the widget takes up, NOT sprite size!
        int w, h;
        // where to start sameline widgets relative to x, y (possibly inside w, h above)
        int endlx, endly;
        // these are used by the default layout: pixels to each side
        // of the current space, -1 indicates no attachment
        int spacel, spacer, spacet, spaceb;
        
        Widget *parent;
        
        //virtual void draw(Window *window) = 0;
        virtual void draw(SDL_Surface *surface) = 0;
        virtual void layout(Window *parent);
        
    public:
        Widget() : takesclicks(false), takeskeys(false), takeskeyfocus(false)
                   , spacel(-1), spacer(-1), spacet(-1), spaceb(-1) {}
        
        enum placement_t { SAMELINE, NEWLINE, NEWCOL, HEADER };
        //virtual placement_t placementType() = 0;
        
        Widget &stretchX() {
            spacel = spacer = 0;
            return *this;
        }
        Widget &stretchY() {
            spacet = spaceb = 0;
            return *this;
        }
        Widget &leftMargin(int x) {
            spacel = x;
            return *this;
        }
        Widget &rightMargin(int x) {
            spacer = x;
            return *this;
        }
        Widget &topMargin(int x) {
            spacet = x;
            return *this;
        }
        Widget &bottomMargin(int x) {
            spaceb = x;
            return *this;
        }
    
};

void drawrect(SDL_Surface *surface, int x1, int y1, int x2, int y2, int c);
SDL_Surface *createrect(int w, int h, Uint8 r, Uint8 g, Uint8 b);

class BorderWidget : public Widget {
    private:
        void gradline(SDL_Surface *surface, int x, int y, int w, int h, int c1, int c2);
    protected:
        int colour1, colour2;
        int tabsize;  // 1 means no tab
        
    public:
        BorderWidget() : colour1(0xff9080), colour2(0x901010), tabsize(1) {}
        
        //placement_t placementType();   // deprec
        void draw(SDL_Surface *surface);
        void layout(Window *parent);
    
};

class TextWidget : public Widget {
    private:
        // A textfields field size can only be set via constructor!
        CTextField *textf;
        string str;
        
        void destroy() { if (textf) delete textf; }
    public:
        TextWidget() : textf(NULL) {}
        TextWidget(const char *_str) : textf(NULL), str(_str) {}
        ~TextWidget() { destroy(); }
        
        void changeStr(const char *_str) { str = _str; }
        
        void draw(SDL_Surface *surface);
        void layout(Window *parent);
    
};


class Button : public Widget, public CMouseInputHandler {
    private:
        bool indented;
        
        int borderc1, borderc2, normalc, indentc;
        void pressed();
    public:
        Button();
        
        void draw(SDL_Surface *surface);
        // uses default layout
        //void layout(Window *parent);
        
        virtual bool leftPress(int x, int y);
        virtual bool leftRelease(int x, int y);
        virtual bool leftDrag(int x, int y);
        virtual bool rightRelease(int x, int y);
};

class Graph : public Widget {
    private:
        struct GraphingData {
            float xscale, yscale;
            int numpoints;
            int *data;
            GraphingData(int points, int *ptr) : numpoints(points), data(ptr) {}
        };
        vector<GraphingData> graphs;
    public:
        Graph() { spacel = spacer = spacet = spaceb = 5;}
        void addData(int points, int *ptr) {
            graphs.push_back(GraphingData(points, ptr));
        }
        
        void draw(SDL_Surface *surface);
        void layout(Window *parent);
};


#endif
