// should only be called on type 3 widgets
void Widget::draw(SDL_Surface *surface) {
    //surface ignored
//    if (needsredraw) {    // call in WM instead to set spritemaster?
//    }
    rendersprite->draw(position);
    //drawChildren:
    Widget *temp = drawchild;
    while (temp) {
        temp->draw();
        temp = temp->drawnext;
    }
}



//type 1
//needsredraw not used, but is reset so that setNeedRedraw() can propergates up tree
void Widget::redraw(SDL_Surface *surface, Widget *spritemaster) {
    render(surface);
    for (vector<Widget *>::iterator it = widgetlist.begin(); it < widgetlist.end(); it++) {
        cout << "adding widget" << endl;

        (*it)->layout(this);
        (*it)->redraw(surface, spritemaster);
    }
    needsredraw = false;
}

//type 2
void Widget::redraw(SDL_Surface *surface, Widget *spritemaster) {
    if (needsredraw || !rendersurface) {
        if (rendersurface)
            SDL_FreeSurface(rendersurface);
        rendersurface = createrect(position.w, position.h, backr, backg, backb);
        
        render(rendersurface);
        for (vector<Widget *>::iterator it = widgetlist.begin(); it < widgetlist.end(); it++) {
            cout << "adding widget" << endl;
    
            (*it)->layout(this);
            (*it)->redraw(rendersurface, spritemaster);
        }
        needsredraw = false;
    }
    
    SDL_BlitSurface(rendersurface, NULL, surface, &position);
}


//type 3
//surface ignored
void Widget::redraw(SDL_Surface *surface, Widget *spritemaster) {
    if (needsredraw) {
        drawchild = drawlast = NULL;
    
        SDL_Surface *tempsurface = createrect(position.w, position.h, backr, backg, backb);
        
        render(tempsurface);
        for (vector<Widget *>::iterator it = widgetlist.begin(); it < widgetlist.end(); it++) {
            (*it)->layout(this);
            (*it)->redraw(tempsurface, this);
        }
        loadSprite(tempsurface);
        
        needsredraw = false;
    }
    if (spritemaster) {
        if (!spritemaster->drawchild)
            spritemaster->drawchild = this;
        else
            spritemaster->drawlast->drawnext = this;
        spritemaster->drawlast = this;
        drawnext = NULL;
    }
}
    
    
void SomeSortOfWidget::render(SDL_Surface *surface) {
    cout << "Window::redraw(): colour r"<< backr << " g" << backg << " b" << backb << endl;
    SDL_Surface *tempsurface;
    vector<Widget *>::iterator it;
    
    //linex = liney = 0;
    //nextlinex = nextliney = 0;
    //nextcolx = headerh = 0;
    
    frame.x1 = frame.y1 = 0;
    frame.x2 = position.w;
    frame.y2 = position.h;
    nextcolx = 0;
    
    tempsurface = createrect(position.w, position.h, backr, backg, backb);
    cout << "tempsur = " << tempsurface << endl;
    
    for (it = widgetlist.begin(); it < widgetlist.end(); it++) {
        cout << "adding widget" << endl;
        //positionWidget(*it);
        (*it)->layout(this);
        (*it)->draw(tempsurface);
        //(*it)->draw(this);
        //(*it)->needupdate = false;
    }
        
    loadSprite(tempsurface);
    needsredraw = false;
}
