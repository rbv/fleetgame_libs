#include <io/general.h>
#include <gui/wm.h>
#include <io/video2.h>
#include <io/text.h>


#include <stdlib.h>

unsigned int randomseed;

signed int random(int low, int high) {
    randomseed += (~randomseed + 1001) * 3021377 ^ randomseed >> 11 ^ randomseed >> 5;
    return randomseed % (high - low + 1) + low;
    //return rand() % (high - low + 1) + low;
    //return (int)((float)rand() * (high - low + 1)) / RAND_MAX + low;
}

int dice(int num, int sides, int add = 0) {
    int result = 0, ab;
    
    for (int i = 0; i < num; i++) {
        //result += rand() % sides;
        //ab = 1 + (int)((float)rand() * (float)sides) / RAND_MAX;
        ab = random(1, 6);
        result += ab;
    }
    return result + add;
}

class TestBox1 : public Window {
    private:
        BorderWidget border;
        TextWidget text;
        TextWidget text2;
        MoveableWindowControl moveable;
        Button button, button2;
        
        int shakectr;
        RectBlock waspos;
        
        void redraw() {
            cout << "TextBox redraw" << endl;
            Window::redraw();
        }
        
        void sendAlert(void *sender, int arg) {
            if (sender == &button) {
                shakectr = 15;
                needsattention = true;
                waspos = position;
            }
        }
        
        void update() {
            if (shakectr--) {
                position = waspos;
                if (!shakectr)
                    return;
                needsattention = true;
                position.x += random(-15, 15);
                position.y += random(-15, 15);
            }
        }
        
    public:
        TestBox1() : text("Greetings!"), text2("You have no chance to survive, make your time!")
                     , moveable(this) {
            addWidget(border);
            addWidget(text);
            addWidget(text2);
            addWidget(button);
            addWidget(button2.stretchX().stretchY());

            position.w = 200;
            position.h = 150;
            name = "testbox";
            place(100, 250);
            
            shakectr = 0;
        }
};

class StatsBox : public Window {
    private:
        //Player p;
        BorderWidget border;
        TextWidget line1, line2;
        
    public:
        StatsBox () {
            //p.rollStats();
            char buffer[1000];
            line1.changeStr("Player the Human");
            //sprintf(buffer, "HP:%d/%d STR:%d CON:%d PWR:%d DEX:%d", p.hp.cur, p.hp.max, p.str.cur, p.con.cur, p.pwr.cur, p.dex.cur);
            line2.changeStr(buffer);
            addWidget(border);
            addWidget(line1);
            addWidget(line2);
            position.w = 400;
            position.h = 50;
            place(200, 520);
        }
    
};

class DiceGraph : public Window {
    private:

        BorderWidget border;
        Graph graph;

        int dicecount[64], countcount[50], binoprob[50];

        void calc() {
            int stime = SDL_GetTicks();
            int i, j, k, sum = 0;
            for (i = 0; i < 51; i++)
                countcount[i] = 0;
            for (i = 0; i < 200000; i++) {
                sum = 0;
                for (j = 0; j < 50; j++) {
                    if (random(0,63) < 32) {
                        sum++;
                    }
                }
                if (sum == 0)
                 cout << "sum = 0!\n";
                countcount[sum]++;
            }
            
            for (i = 0; i < 51; i++) {
                double prob = 1.0f;
                binoprob[i] = 0;
                for (j = i + 1; j <= 50; j++)
                    prob *= j;
                for (j = 1; j <= 50 - i; j++)
                    prob /= j;
//                prob *= pow(0.5, 50);
                prob *= 8.882e-17;
                binoprob[i] = (unsigned int)(10000000000.0*prob);
            }
                
            for (i = 0; i < 64; i++)
                dicecount[i] = 0;
            for (i = 0; i < 1000000; i++)
                dicecount[random(0,63)]++;
            cout << "rolls in time: " << SDL_GetTicks() - stime << endl;
            for (i = 0; i < 64; i++)
                cout << i << " = " << dicecount[i] << endl;
            for (i = 2; i < 64; i<<=1) {
                for (k = 0; k < i; k++) {
                    for (sum = 0, j = 0; j < 64; j += i)
                        sum += dicecount[j + k];
                    //cout << "sum at every " << i << " mod " << k << " = " << sum << endl;
                }
            }
        }
        
    public:
        DiceGraph () {
            calc();
            addWidget(border);
            //graph.stretchX().stretchX();
            cout << "cc = " << countcount[0] << endl;
            graph.addData(64, dicecount);
            graph.addData(51, countcount);
            graph.addData(51, binoprob);
            cout << "cc = " << countcount[0] << endl;
            addWidget(graph);
            position.w = 400;
            position.h = 400;
            place(300, 100);
        }
    
};

void mainloop() {
    SDL_Surface *tempsurface;

    WindowPane *backdrop = new Window;
   // tempsurface = Window::createrect(video->getWidth(), video->getHeight(), 128, 96, 128);
    tempsurface = SDL_LoadBMP("cthulhuback1.bmp"); 
    //cout << rectangleRGBA(tempsurface, 5,5,
 	//			 100,100, 0xffffff, 0xffffff, 0, 0) << "boooooo\n";
    backdrop->loadSprite(tempsurface);

    backdrop->place(0,0);
    backdrop->name = "backdrop";
    WM::addWindow(backdrop);
    
    TestBox1 widgettedbox;
    WM::addWindow(&widgettedbox);
    
    StatsBox statbox;
    WM::addWindow(&statbox);
    
    DiceGraph graphwindow;
    WM::addWindow(&graphwindow);
    
    /*
    MoveableWindow *charbox = new MoveableWindow;
    tempsurface = createrect(200, 100, 200, 200, 200);
    drawrect(tempsurface, 0, 0, 199, 99, 0x808000); 
    CTextField line;
    //line.changeStr("Drag Me!");
    //SDL_BlitSurface(line.getSurface(), NULL, tempsurface, NULL); 
    charbox->loadSprite(tempsurface);
    charbox->place(100, 100);
    charbox->name = "firstbox";
    WM::addWindow(charbox);
    
    MoveableWindow *secondbox = new MoveableWindow;
    tempsurface = createrect(200, 200, 255, 255, 0);
    line.changeStr("Number Two!");
    SDL_BlitSurface(line.getSurface(), NULL, tempsurface, NULL); 
    secondbox->loadSprite(tempsurface);
    secondbox->place(400, 150);
    secondbox->name = "num 2 box";
    WM::addWindow(secondbox);
    */
    
    WM::debug();
    
    //for(int stime = SDL_GetTicks(); SDL_GetTicks() < stime + 5000; SDL_Delay(15)) {
    int stime = SDL_GetTicks(), ticks = 0;
    while (!input->pollInput()) {
        // logic section
        WM::doUpdates();
        // draw section
        WM::draw();
        video->updateScreen();
        SDL_Delay(20);
        ticks++;
    }
    stime = SDL_GetTicks() - stime;
    cout << ticks << " ticks in " << stime << " ms = " << ((double)ticks * 1000) / stime << " fps" << endl; 

//    delete secondbox;
//    delete charbox;
    delete backdrop;
}


int main(int argc, char *argv[]) {
    cout.setf(ios::unitbuf);
    randomseed = time(0);
    srand(time(0));
    cout << time(0) << endl;
#ifdef __linux__
    //putenv("SDL_VIDEODRIVER=dga");
#endif
    STARTUP

    video = new CSDLVideo;
    video->create();
    input = new CInput;
    text = new CTextEngine;

    mainloop();
    
    delete text;
    delete input;
    delete video;
}
