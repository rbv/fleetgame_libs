#include <string.h>
#include <stdlib.h>

#include "video2.h"

using namespace std;


CSprite::loadmethod_t CSprite::loadmethod;
CSprite::drawmethod_t CSprite::drawmethod;

//////////////////////// CSprite methods //////////////////////

CSprite::CSprite() {
    cout << "Constructing sprite" << endl;
    create();
    if (video->backend == CVideo::SDL)
        ((CSDLSprite *)this)->create();
    else if (video->backend == CVideo::OPENGL)
        ((COGLSprite *)this)->create();
}

CSprite::~CSprite() {
    cout << "Destroying sprite" << endl;
    destroy();
    if (video->backend == CVideo::SDL)
        ((CSDLSprite *)this)->destroy();
    else if (video->backend == CVideo::OPENGL)
        ((COGLSprite *)this)->destroy();
}


//////////////////////// CSDLSprite methods //////////////////////

void CSDLSprite::create() {
    VIDEODBG(cout << "SDLSprite init" << endl;)
    sdl.surface = NULL;
}

void CSDLSprite::destroy() {
    VIDEODBG(cout << "SDLSprite destruct" << endl;)
    if (sdl.surface)
        SDL_FreeSurface(sdl.surface);
}

void CSDLSprite::loadSprite(SDL_Surface *surface, bool usekey, unsigned int colourkey, bool keepsurface) {
    if (!surface)
        return;
    if (usekey)
        SDL_SetColorKey(surface, SDL_SRCCOLORKEY, colourkey);
        
    sdl.surface = SDL_DisplayFormat(surface);
    if (!sdl.surface) {
        cerr << "failed to copy surface from pointer" << SDL_GetError();
        exit(1);
    }
    if (!keepsurface)
        SDL_FreeSurface(surface);
}

void CSDLSprite::drawSprite(SDL_Rect where) {
    SDL_BlitSurface(sdl.surface, NULL, video->screen, &where);
}

void CSDLSprite::drawRect(SpriteFrame &frame, SDL_Rect where) {            
    where.x += frame.shiftx;
    where.y += frame.shifty;
    SDL_Rect dest = {frame.x, frame.y, frame.w, frame.h};    // What??

    SDL_BlitSurface(sdl.surface, &dest, video->screen, &where);
}

//////////////////////// COGLSprite methods //////////////////////

void COGLSprite::create() {
    ogl.texref = 0;
}

void COGLSprite::destroy() {
    glDeleteTextures(1, &ogl.texref);
}

void COGLSprite::loadSprite(SDL_Surface *surface, bool usekey, unsigned int colourkey, bool keepsurface) {
    VIDEODBG(cout << "Loading Sprite from surface with details" << endl;)
    if (!surface) {
        cerr << "bailing, someone passed NULL surface to loadSprite";
        exit(1);
    }

    calcTexSize(surface->w, surface->h);
    
    VIDEODBG(
        cout.setf(ios::hex,ios::basefield);
        cout << "IN surface: Rmask = " << surface->format->Rmask << " Gmask = " << surface->format->Gmask << " Bmask = " << surface->format->Bmask << endl;
        cout << "SCREEN: Rmask = " << video->screen->format->Rmask << " Gmask = " << video->screen->format->Gmask << " Bmask = " << video->screen->format->Bmask << endl;
        cout.setf(ios::dec,ios::basefield);
    )
    
    surfaceToTexture(surface, usekey, colourkey);

    if (!keepsurface)
        SDL_FreeSurface(surface);
}

void COGLSprite::calcTexSize(int w, int h) {
    // find texture size with power of 2 side lengths
    VIDEODBG(cout << "finding tex size. w = " << w << " h = " << h << endl;)
    unsigned int temp = 1;
    while (temp < w) temp <<= 1;
    ogl.texw = temp;
    temp = 1;
    while (temp < h) temp <<= 1;
    ogl.texh = temp;
}

void COGLSprite::surfaceToTexture(SDL_Surface *bmp, bool usealpha, unsigned int colourkey) {
    SDL_Surface *processed;
    
    if (usealpha) {
/*   OLD
        SDL_SetColorKey(bmp, SDL_SRCCOLORKEY, colourkey);

        // Converts to 32bit, translating colourkey to alpha channel
        processed = SDL_DisplayFormatAlpha(bmp);
        
        VIDEODBG(
            cout.setf(ios::hex,ios::basefield);
            cout << "processed surface: Rmask = " << processed->format->Rmask << " Gmask = " << processed->format->Gmask << " Bmask = " << processed->format->Bmask << " Amask = " << processed->format->Amask << endl;
            cout.setf(ios::dec,ios::basefield);
            cout << "bits = " << (int)processed->format->BitsPerPixel << " bytes= " << (int)processed->format->BytesPerPixel << " Ckey= " << (int)processed->format->colorkey << endl;
            cout << "w = " << processed->w << " h = " << processed->h << " pitch = " << (int)processed->pitch << " flags = " << (int)processed->flags << endl;
        )
        
        createTexture((unsigned char*)processed->pixels, GL_RGBA, GL_UNSIGNED_BYTE, 4);
        SDL_FreeSurface(processed);  
*/
//   NEW
        SDL_SetColorKey(bmp, SDL_SRCCOLORKEY, colourkey);
        
        processed = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, ogl.texw, ogl.texh, 32, 0xff0000, 0xff00, 0xff, 0xff000000); 
        if (!processed) {
            cerr << "failed to copy surface to OGL tex load temp" << SDL_GetError();
            exit(1);
        }
        if (processed->flags & SDL_HWSURFACE) {
            cerr << "SDL_CreateRGBSurface returned an alpha hardware surface!" << endl;
        }
    
        //SDL_SetAlpha(processed, SDL_SRCALPHA, 255);
        SDL_BlitSurface(bmp, NULL, processed, NULL);
//   FIN

        createTexture((unsigned char*)processed->pixels, GL_RGBA, GL_UNSIGNED_BYTE, 4);
    } else {
/*   OLD
        //processed = SDL_DisplayFormat(bmp);  //doesn't work?
        invert((unsigned char*)bmp->pixels, bmp->w * bmp->h); 
        
        createTexture((unsigned char*)bmp->pixels, GL_RGB, GL_UNSIGNED_BYTE, 3);
*/
//   NEW
        processed = SDL_CreateRGBSurface(SDL_SWSURFACE, ogl.texw, ogl.texh, 24, 0xff, 0xff00, 0xff0000, 0); 
        if (!processed) {
            cerr << "failed to copy surface to OGL tex load temp" << SDL_GetError();
            exit(1);
        }
    
        SDL_BlitSurface(bmp, NULL, processed, NULL);

        createTexture((unsigned char*)processed->pixels, GL_RGB, GL_UNSIGNED_BYTE, 3);
    }
    SDL_FreeSurface(processed);
    ogl.blend = !!usealpha;
}

void COGLSprite::invert(unsigned char *ptr, int len) {
    unsigned char temp;
    for (int i = len; i; --i) {
        temp = ptr[0];
        ptr[0] = ptr[2];
        ptr += 2;
        *ptr++ = temp;
    }
}

void COGLSprite::createTexture(unsigned char *pixels, GLenum format, GLenum type, GLint components) {
    glGenTextures(1, &ogl.texref);
    glBindTexture(GL_TEXTURE_2D, ogl.texref);
    
    VIDEODBG(cout << "loading texture";)
    glTexImage2D(GL_TEXTURE_2D, 0, components, ogl.texw, ogl.texh, 0, format, type, pixels);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	VIDEODBG(cout << "...done" << endl;)
}

void COGLSprite::drawSprite(SDL_Rect where) {
    // where.w and where.h specify final size, if 0 use default
    if (!where.w)
        where.w = ogl.texw;
    if (!where.h)
        where.h = ogl.texh;
        
    if (ogl.blend)
        glEnable(GL_BLEND);
    glLoadIdentity();
    glBindTexture(GL_TEXTURE_2D, ogl.texref);
    glTranslatef(where.x, where.y, 0);
    glBegin(GL_QUADS); 
        glTexCoord2f(1.0f, 1.0f);    glVertex3f(where.w, where.h, 0.0f);
        glTexCoord2f(0.0f, 1.0f);    glVertex3f(0.0f, where.h, 0.0f);
        glTexCoord2f(0.0f, 0.0f);    glVertex3f(0.0f, 0.0f, 0.0f);
        glTexCoord2f(1.0f, 0.0f);    glVertex3f(where.w, 0.0f, 0.0f);
    glEnd();
    if (ogl.blend)
        glDisable(GL_BLEND);
}

// This is actually for CSpriteSheet
void COGLSprite::drawRect(SpriteFrame &frame, SDL_Rect where) {
    float tex_l, tex_r, tex_t, tex_b;

    tex_l = (float)frame.x/(float)ogl.texw;
    //tex_r = tex_l + ((float)frame.w - 1)/(float)texw;
    tex_r = tex_l + (float)frame.w /(float)ogl.texw;
    tex_t = (float)frame.y/(float)ogl.texh;
    //tex_b = tex_t + ((float)frame.h - 1)/(float)texh;
    tex_b = tex_t + (float)frame.h/(float)ogl.texh;
    
    // where.w and where.h specify final size, if 0 use default
    if (!where.w)
        where.w = frame.w;
    if (!where.h)
        where.h = frame.h;
        
    VIDEODBG(
        cout << "drawing spritesheet x = " << where.x + frame.shiftx << " y = " << where.y + frame.shifty << " w = " << where.w << " h = " << where.h << endl;
        cout << "tex left = " << tex_l << " tex right = " << tex_r << " tex top = " << tex_t << " tex bottom = " << tex_b << endl;
        cout << "frame w = " << frame.w << " frame h = " << frame.h << endl;
    )
    
    if (ogl.blend)
        glEnable(GL_BLEND);
    glLoadIdentity();
    glBindTexture(GL_TEXTURE_2D, ogl.texref);
    glTranslatef(where.x + frame.shiftx, where.y + frame.shifty, 0);
    glBegin(GL_QUADS); 
        glTexCoord2f(tex_r, tex_b);    glVertex3f(where.w, where.h, 0.0f);
        glTexCoord2f(tex_l, tex_b);    glVertex3f(0.0f, where.h, 0.0f);
        glTexCoord2f(tex_l, tex_t);    glVertex3f(0.0f, 0.0f, 0.0f);
        glTexCoord2f(tex_r, tex_t);    glVertex3f(where.w, 0.0f, 0.0f);
    glEnd();
    if (ogl.blend)
        glDisable(GL_BLEND);
}

