#include <iostream>
#include <vector>
#include <sstream>

#include "text.h"

using namespace std;

CTextEngine *text;

//////////////////// CTextEngine methods ////////////////////

CTextEngine::CTextEngine() {
    cout << "Loading text engine" << endl;
    
    if (TTF_Init() == -1) {
        cerr << "Could not initialise SDL_TTF. Error: " << TTF_GetError();
        exit(-1);
    }
    deffont = TTF_OpenFont("airstrip.ttf", 14);
    if (!deffont)
        cerr << "Could not load def font: " << TTF_GetError();
}

CTextEngine::~CTextEngine() {
    cout << "Shutting down text engine" << endl;
    if (deffont)
        TTF_CloseFont(deffont);
    TTF_Quit();
}

SDL_Surface *CTextEngine::printText(const char *text) {
    SDL_Color col = {255, 255, 255, 0};
    return TTF_RenderText_Solid(deffont, text, col);
}



//////////////////// CTextField methods ////////////////////

CTextField::CTextField() : inserter(str) {
    rendered = false;
    dontrender = false;
    surface = NULL;
    sheet = NULL;
    color.r = 255;
    color.g = 255;
    color.b = 255;
    font = text->deffont;
    fieldheight = -1;
    fieldwidth = video->getWidth();
}

CTextField::CTextField(const string &str, int fieldwidth, int fieldheight) : 
            str(str), fieldwidth(fieldwidth), fieldheight(fieldheight), inserter(str) {
    dontrender = false;
    surface = NULL;
    sheet = NULL;
    color.r = 255;
    color.g = 255;
    color.b = 255;
    font = text->deffont;
    render();
}

CTextField::CTextField(const char *str, SDL_Color &color, int fieldwidth, int fieldheight) : 
            str(str), fieldwidth(fieldwidth), fieldheight(fieldheight), inserter(str) {
    dontrender = false;
    surface = NULL;
    sheet = NULL;
    this->color = color;
    font = text->deffont;
    render();
}

CTextField::~CTextField() {
    wipe();
}

void CTextField::wipe() {
cout <<"textfield wipe...";
    if (sheet) {
        delete sheet;
        sheet = NULL;
    } /*else {
        // <s>sheet given pointer to surface. It assumes control of it and will delete it itself</s>
        if (surface)
            SDL_FreeSurface(surface);
        surface = NULL;
    }*/
//ugly, find better solution later. tired
//#ifndef FLEET_VIDEO_SDL
    if (surface) {
        SDL_FreeSurface(surface);
        surface = NULL;
    }
//#endif
cout<<"fin\n";
}

void CTextField::render() {
    int w, h, ad, lineskip = TTF_FontLineSkip(font);
    // start is pointer to the part of the string still to be to processed (split)
    char fragment[100];  
    const char *start, *find;
    int i = 0, total, lines = 1; // i is offset from start
    vector<SDL_Surface *> r_lines;  // rendered lines
    SDL_Rect dest;  // for blitting each line to the final surface
    SDL_Surface *temp;

    wipe();
        
    TXTDBG(cout << "rendering text: \"" << str << "\" with fieldwith " << fieldwidth << " and lineskip " << lineskip << endl;)
    
    start = str.c_str();
    for (;;) {
        TXTDBG(cout << "==loop for line" << lines << " text is: " << start << endl;)
        TTF_SizeText(font, start, &w, &h);
        TXTDBG(cout << "width is " << w << endl;)
        // by default assume chopping has to occur
        if (w <= fieldwidth) {
            // no chopping needed
            temp = TTF_RenderText_Solid(font, start, color);
            if (temp)
                r_lines.push_back(temp);
            break;
        }
        total = 0;
        while (total < fieldwidth) {
            TTF_GlyphMetrics(font, start[i], NULL, NULL, NULL, NULL, &ad);
            total += ad;
            i++;
        }
        TXTDBG(cout << "i is " << i << endl;)
        
        // find the last space before cutoff point
        find = start + i;
        while (*find != ' ' && find > start + i / 2) // the idea being that it doesn't put 1 letter on a line?
            find--;
        if (find != start + i / 2) {
            i = find - start;
            i++; // skip the space
        }
        TXTDBG(cout << "new i is " << i << endl;)
        
        /*  //  string object-based code I didn't use
        split = s.substr(start, i).s.rfind(' ');
        if (split != string::npos)
            i = split;
        */
        
        strncpy(fragment, start, i);
        fragment[i] = 0; //unneeded?
        if (fieldheight != -1 && (lines + 1) * lineskip > fieldheight) {
            // last line
            strcpy(fragment + i - 1, "...");
            temp = TTF_RenderText_Solid(font, fragment, color);
            if (temp)
                r_lines.push_back(temp);
            break;
        }
        lines++;
    
        start = &start[i];
        i = 0;
        temp = TTF_RenderText_Solid(font, fragment, color);
        if (temp)
            r_lines.push_back(temp);
    }
    
    // Now patch all the lines together! :(
    TXTDBG(cout << "total lines: " << lines << "vector size: " << r_lines.size() << endl;)
    
    // total is width of the new surface
    for (i = 0, total = 0; i < r_lines.size(); i++) {
        //cout << (int)r_lines[i] << endl;
        if (r_lines[i]->w > total) 
            total = r_lines[i]->w;
    }
    TXTDBG(cout << "total is: " << total << flush<< endl;)
    // I don't know what the last 4 0's here are for
    surface = SDL_CreateRGBSurface(SDL_SWSURFACE, total, lines * lineskip + 1, 8, 0, 0, 0, 0);
    SDL_SetColorKey(surface, SDL_SRCCOLORKEY | SDL_RLEACCEL, 0);
    //SDL_SetColorKey(surface, SDL_SRCCOLORKEY, 0);
    
    //SDL_Color blk = {0, 0, 0, 0};
    //for(int i = 0; i < 256; i++) { surface->format->palette->colors[i] = blk; }
    surface->format->palette->colors[1] = color;
    
    dest.x = 0;
    dest.y = r_lines.size() * lineskip;
    for (i = r_lines.size() - 1; i >= 0; i--) {
        temp = r_lines.back();
        dest.y -= lineskip;
        SDL_BlitSurface(temp, NULL, surface, &dest);
        SDL_FreeSurface(temp);
        r_lines.pop_back();
    }
    
    TXTDBG(cout << surface->format->palette->colors[1].r << " " << surface->format->palette->colors[1].g << surface->format->palette->colors[1].b << endl;)
        
    /*   Remanants of old incomplete code taking kerning (kernaling?) into account
    strcpy(fragment, str.c_str());
    TTF_SizeText(font, str.c_str(), &w, &h);
    while (w > fieldwidth && (lineno + 1) * lineskip <= fieldheight) {
	//lines++;

	
	len = len * fieldwidth / w;

        TTF_SizeText(font, fragment, &w, &h);
    } 
    */

    rendered = true;
}

void CTextField::changeStr(const string &str) {
    this->str = str;
    if (!dontrender)
        render();
}

void CTextField::changeStr(const char *str) {
    this->str = str;
    if (!dontrender)
        render();
}

void CTextField::changeStr(int number) {
    stringstream temp;
    temp << number;
    this->str = temp.str();
    if (!dontrender)
        render();
}

void CTextField::draw(SDL_Rect &where) {
    if (!sheet && surface) {
        //sheet = new CSpriteSheet((unsigned char *)surface->pixels, surface->w * surface->h, surface->w, surface->h);
        sheet = new CSpriteSheet;
        //sheet->loadSheet(surface, true, 0, surface->w, surface->h);
        sheet->loadSheet(surface, true, 0, true);
        //sheet->loadSheet(surface, false, 0, surface->w, surface->h);
    }
    if (sheet)
        sheet->drawFrame(where);
}

CTextField &CTextField::clear() {
    str.clear();
    if (!dontrender)
        render();
    return *this;
}

void CTextField::pause() {
    dontrender = true;   
}

void CTextField::resume() {
    dontrender = false;
    render();
}

/* this is for debug */
void CTextField::strout() {
    cout << endl << "string length:" << str.length() << endl;
    for (int i = str.length(); i > 0; i--) {
        int ad, mx, w;
        cout << "char " << i << " is " << str[i - 1] << " width ";
        TTF_GlyphMetrics(font, str[i - 1], NULL, &mx, NULL, NULL, &ad);
        cout << ad << " (with maxx " << mx << ") text length is ";
        TTF_SizeText(font, str.c_str(), &w, NULL);
        cout << w << endl;
        str[i-1] = 0;
    }
}
    



