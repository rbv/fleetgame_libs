#ifndef TEXT_H
#define TEXT_H


#if 0
#define TXTDBG(A) A
#else
#define TXTDBG(A) ;
#endif

#include <string>
#include <iostream>
#include <sstream>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "video2.h"
using namespace std;

/* The plan right now is that this class keeps track of...
 * actually, lets wait for the gui class. maybe that should
 * keep track of all the text onscreen
*/
class CTextEngine {
    private:
        SDL_Surface *printText(const char *text);
    
        //SDL_Surface *testing;
    public:
        TTF_Font *deffont;
        
        CTextEngine();
        ~CTextEngine();
        
        // use unclear
        //void display();
    
};

/* text renderer using SDL_ttf, which uses FreeType
 * automatically wraps lines, manages field size, displays text
 */
class CTextField {
    protected:
        string str;
        ostringstream inserter;
        SDL_Color color;
        SDL_Surface *surface;  // sheet is actually given this surface, 
        CSpriteSheet *sheet;  // this is what is actually used to display
	    TTF_Font *font;
        bool rendered;  // explicitly needed?
        bool dontrender;  
        int fieldwidth;
        int fieldheight;
        
        // delete rendered
        void wipe();
        void render();
    public:
        CTextField();
        CTextField(const string &str, int fieldwidth, int fieldheight = -1);
        CTextField(const char *str, SDL_Color &color, int fieldwidth, int fieldheight = -1);
        ~CTextField();
        
        void draw(SDL_Rect &where);
        SDL_Surface *getSurface() { return surface; }  // kept for purposes other than drawing (?)
        
        void changeStr(const string &str);
        void changeStr(const char *str);
        /* shortcut functions! quick debug! */
        void changeStr(int number);
         
        const string &getStr() { return str; }
        
        void setColor(SDL_Color &color) { this->color = color; }
        
        /* clear string (and rendered) */
        CTextField &clear();
        
        /* Pause and resume rendering, when changing string frequently but don't
         * want to blit it immediately
         */
        void pause();
        void resume();
        
        // WHAT! this is useless!
        template<typename T>
        CTextField &operator <<(T data) {
            inserter.str(str);
            inserter << data;
            str = inserter.str();
            if (!dontrender)
                render();
            return *this;
        }
        
        int getHeight() { return rendered ? surface->h : 0; }
        int getWidth() { return rendered ? surface->w : 0; }
        
        void strout();  //debug purposes
};


extern CTextEngine *text;

#endif
