#include <string.h>
#include <stdlib.h>

#include "video2.h"

using namespace std;


CVideo *video;


//////////////////////// CSDLVideo methods //////////////////////

void CSDLVideo::create() {
    cout << "creat()ing SDL window" << endl;
    if (SDL_InitSubSystem(SDL_INIT_VIDEO))
    {
        cerr << "SDL failed to init!" << SDL_GetError();
        exit(-1);
    }
    //screen = SDL_SetVideoMode(width, height, 16, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN * fullscr);
    screen = SDL_SetVideoMode(width, height, 32, SDL_FULLSCREEN * fullscr);
    if (!screen)
    {
        cerr << "SetVideoMode failed!" << SDL_GetError();
        exit(-1);
    }
    SDL_WM_SetCaption(windowname, NULL);
    
    printInfo();
}

void CSDLVideo::updateScreen() {
    SDL_Flip(screen);
//    SDL_UpdateRect(screen, 0, 0, 0, 0);
}

void CSDLVideo::updateRects(int numrects, SDL_Rect *rects) {
    SDL_UpdateRects(screen, numrects, rects);
}

void CSDLVideo::clearScreen(SDL_Color &color) {
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, color.r, color.g, color.b));
}

void CSDLVideo::drawBox(RectPoints &box, SDL_Color &colour) {
    SDL_Rect rect = box, squareseg;
    unsigned int c = SDL_MapRGB(screen->format, colour.r, colour.g, colour.b);
    squareseg.x = rect.x;
    squareseg.y = rect.y;
    squareseg.w = rect.w;
    squareseg.h = 1;
    SDL_FillRect(screen, &squareseg, c);
    squareseg.h = rect.h;
    squareseg.w = 1;
    SDL_FillRect(screen, &squareseg, c);
    squareseg.x = rect.x + rect.w;
    SDL_FillRect(screen, &squareseg, c);
    squareseg.x = rect.x;
    squareseg.h = 1;
    squareseg.w = rect.w;
    squareseg.y = rect.y + rect.h;
    SDL_FillRect(screen, &squareseg, c);
}

//////////////////////// COGLVideo methods //////////////////////

void COGLVideo::create() {   
    cout << "creat()ing OpenGL window" << endl;
    if (SDL_InitSubSystem(SDL_INIT_VIDEO)) {
        cerr << "SDL video failed to init!" << SDL_GetError();
        exit(-1);
    }
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    screen = SDL_SetVideoMode(width, height, 32, SDL_OPENGL | SDL_FULLSCREEN * fullscr);
    if (!screen) {
        cerr << "SetVideoMode failed!" << SDL_GetError();
        exit(-1);
    }
    SDL_WM_SetCaption(windowname, NULL);
    
    /* openGL setup */
    
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

    //glOrtho(0.0, 800.0, 0.0, 600.0, 0.0, 1.0);
    glOrtho(0.0, 800.0, 600.0, 0.0, 0.0, 1.0);  //correct!
    glViewport(0, 0, width, height);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();	
	
	//glShadeModel(GL_SMOOTH);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	//glClearDepth(1.0f);
	glDisable(GL_DEPTH_TEST);
	//glDepthFunc(GL_LEQUAL);	
    //glClear(GL_DEPTH_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    int numBuffers;
    glGetIntegerv(GL_AUX_BUFFERS, &numBuffers);
    cout << "BUFFERS: " << numBuffers << endl;

}

void COGLVideo::updateScreen() {
    SDL_GL_SwapBuffers();
}

void COGLVideo::updateRects(int numrects, SDL_Rect *rects) {
    SDL_GL_SwapBuffers();
}

void COGLVideo::clearScreen(SDL_Color &color) {
    glClearColor(color.r, color.g, color.b, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);  //  | GL_DEPTH_BUFFER_BIT
}

void COGLVideo::drawBox(RectPoints &box, SDL_Color &c) {   
    glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);
    glDisable(GL_TEXTURE_2D);

    glLoadIdentity();
    
    glColor3f(c.r/256.0f, c.g/256.0f, c.b/256.0f);
    glBegin(GL_LINE_LOOP);
        glVertex3f(box.x1, box.y1, 0.0f);
        glVertex3f(box.x1, box.y2, 0.0f);
        glVertex3f(box.x2, box.y2, 0.0f);
        glVertex3f(box.x2, box.y1, 0.0f);
    glEnd();   
    glPopAttrib();
}

//////////////////////// CVideo_BASE methods //////////////////////

CVideo::CVideo() {
    cout << "Loading video module" << endl;
    
    backend = NOINIT;
    
    screen = NULL;
    fullscr = false;
    width = 800;
    height = 600;
    strcpy(windowname, "");
}

CVideo::~CVideo() {
    cout << "Shutting down graphics module" << endl;
    
    close();
}

void CVideo::close() {
    if (SDL_WasInit(SDL_INIT_VIDEO)) {
        cout << "Shutting down SDL graphics subsystem" << endl;
        SDL_QuitSubSystem(SDL_INIT_VIDEO);
    }
}

void CVideo::setBackend(backend_t type) {
    backend = type;
    if (type == NOINIT) return;
    if (type == SDL) {
        CSprite::loadmethod = (CSprite::loadmethod_t)&CSDLSprite::loadSprite;
        CSprite::drawmethod = (CSprite::drawmethod_t)&CSDLSprite::drawSprite;
        CSpriteSheet::drawmethod = (CSpriteSheet::drawmethod_t)&CSDLSprite::drawRect;
    }
    if (type == OPENGL) {
        CSprite::loadmethod = (CSprite::loadmethod_t)&COGLSprite::loadSprite;
        CSprite::drawmethod = (CSprite::drawmethod_t)&COGLSprite::drawSprite;
        CSpriteSheet::drawmethod = (CSpriteSheet::drawmethod_t)&COGLSprite::drawRect;
    }
    VIDEODBG(cout << "set type to " << backend << "; drawmethod = " << *(int *)&CSprite::drawmethod << endl;)
}

void CVideo::switchType(backend_t type) {
    if (type == backend) return;

    if (type == SDL) 
        video = new CSDLVideo(*this);
    if (type == OPENGL) 
        video = new COGLVideo(*this);
    
    delete this;
    
    video->setBackend(type);
}

void CVideo::printInfo() {
    const SDL_VideoInfo *info = SDL_GetVideoInfo();
    cout << "Video Info:" << endl;
    cout << " HW surface available: " << info->hw_available << endl;
    cout << " WindowManger available: " << info->wm_available << endl;
    cout << " HW to HW blit accel: " << info->blit_hw << endl;
    cout << " HW2HW colour-keyed blit accel: " << info->blit_hw_CC << endl;
    cout << " HW2HW alpha blit accel: " << info->blit_hw_A << endl;
    cout << " SW to HW blit accel: " << info->blit_sw << endl;
    cout << " SW2HW colour-keyed blit accel: " << info->blit_sw_CC << endl;
    cout << " SW2HW alpha blit accel: " << info->blit_sw_A << endl;
    cout << " Colour fill accel: " << info->blit_fill << endl;
    cout << " VMemory, KB: " << info->video_mem << endl;
    
    if (backend == SDL) {
        cout << " screen flags = " << hex << screen->flags << dec << endl;
        cout << " bits = " << (int)screen->format->BitsPerPixel << endl;
        cout << hex << " r = " << screen->format->Rmask << " g = " << screen->format->Gmask << " b = " << screen->format->Bmask << " a = " << screen->format->Amask << endl;
        cout << dec;
    }
}

