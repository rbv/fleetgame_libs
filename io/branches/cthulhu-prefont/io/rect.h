/* SDL_Rect is not appropriate in many cases. Also it uses shorts and unsigned
 * width and height. These two replacement structures should be happily interchangable
 */
 
#ifndef RECT_H
#define RECT_H
 
// for SDL_Rect
#include <SDL/SDL_video.h>
 
struct RectBlock {
    int x;
    int y;
    int w;
    int h;
    
    RectBlock () : 
        x(0),
        y(0),
        w(0),
        h(0)
        {}
    
    RectBlock (int x, int y, int w, int h) : 
        x(x),
        y(y),
        w(w),
        h(h)
        {}
        
    RectBlock (const SDL_Rect &that) :
        x(that.x),
        y(that.y),
        w(that.w),
        h(that.h)
        {}
        
    operator SDL_Rect() {
        SDL_Rect temp = {
            x,
            y,
            w,
            h
        };
        // SDL_Rect can't handle negative w or h
        if (w < 0) {
            temp.x = x + w;
            temp.w = -w;
        }
        if (h < 0) {
            temp.y = y + h;
            temp.h = -h;
        }
        return temp;
    }
};

struct RectPoints {
    int x1;
    int y1;
    int x2;
    int y2;
    
    RectPoints () :
        x1(0),
        y1(0),
        x2(0),
        y2(0)
        {}
    
    RectPoints (int x1, int y1, int x2, int y2) : 
        x1(x1),
        y1(y1),
        x2(x2),
        y2(y2)
        {}
    
    RectPoints (const RectBlock &that) :
        x1(that.x),
        y1(that.y),
        x2(that.x + that.w),
        y2(that.y + that.h)
        {}
    
    RectPoints (const SDL_Rect &that) :
        x1(that.x),
        y1(that.y),
        x2(that.x + that.w),
        y2(that.y + that.h)
        {}
        
    operator RectBlock() {
        return RectBlock(
            x1,
            y1,
            x2 - x1,
            y2 - y1
            );
    }
    
    operator SDL_Rect() {
        SDL_Rect temp = {
            x1,
            y1,
            x2 - x1,
            y2 - y1
        };
        // SDL_Rect can't handle negative w or h
        if (x1 > x2) {
            temp.x = x2;
            temp.w = x1 - x2;
        }
        if (y1 > y2) {
            temp.y = y2;
            temp.h = y1 - y2;
        }
        return temp;
    }
};

/* is a point within a Rect? */
inline bool inRect(int x, int y, RectBlock rect) {
    return x >= rect.x && y >= rect.y && x < rect.x + rect.w && y < rect.y + rect.h;
}

/*
inline bool rectsIntersect(RectPoints rect1, RectPoints rect2) {
    //rect1.x1 <= rect2.x2 && rect1.x2 >= rect2.x1
    cout << "In rectIntersect\n";
    return
    rect1.x1 < rect2.x1 ^ rect1.x1 < rect2.x2 || rect2.x1 < rect2.x1 ^ rect1.x2 < rect2.x2 &&
    rect1.y1 < rect2.y1 ^ rect1.y1 < rect2.y2 || rect2.y1 < rect2.y1 ^ rect1.y2 < rect2.y2;
}
*/

inline bool rectsIntersect(SDL_Rect rect1, SDL_Rect rect2) {
    RectPoints r1(rect1), r2(rect2);
    
    //return ((r1.x1 >= r2.x1 && r1.x1 <= r2.x2) || (r1.x2 >= r2.x1 && r1.x2 <= r2.x2))
     //      && ((r1.y1 >= r2.y1 && r1.y1 <= r2.y2) || (r1.y2 >= r2.y1 && r1.y2 <= r2.y2));
    return (r1.x1 <= r2.x2 && r1.x2 >= r2.x1) && (r1.y1 <= r2.y2 && r1.y2 >= r2.y1);
    
}

#endif
