#include <string.h>
#include <stdlib.h>

#include "video2.h"

using namespace std;


CSpriteSheet::drawmethod_t CSpriteSheet::drawmethod;

//////////////////// CSpriteSheet methods ////////////////////

CSpriteSheet::CSpriteSheet() {
    sheetw = sheeth = 0;
    numactions = 0;
}

CSpriteSheet::~CSpriteSheet() {
    // destroy framelist
    for (int i = 0; i < numactions; ++i)
        delete [] framelist[i].frames;
    delete [] framelist;
}

void CSpriteSheet::loadSheet(SDL_Surface *surface, bool usekey, unsigned int colourkey, bool keepsurface) {
    sheetw = surface->w;
    sheeth = surface->h;
    buildDefaultFramelist();
    load(surface, usekey, colourkey, keepsurface);
}

void CSpriteSheet::loadSheet(unsigned char *source, bool usekey, unsigned int colourkey, int bufsize, int framew, int frameh, int rowsize) {
    SDL_RWops *rwops_wrap = SDL_RWFromMem(source, bufsize);
    SDL_Surface *bmp = SDL_LoadBMP_RW(rwops_wrap, 1);
    if (!bmp) {
        cerr << "failed to load bmp surface from buffer";
        exit(1);
    }
    
    sheetw = bmp->w;
    sheeth = bmp->h;
    buildDefaultFramelist(framew, frameh, rowsize);
    load(bmp, usekey, colourkey);
}

void CSpriteSheet::loadSheet(const char *filename, bool usekey, unsigned int colourkey, int framew, int frameh, int rowsize) {
    SDL_Surface *bmp;    
    bmp = SDL_LoadBMP(filename);
    if (!bmp) {
        cerr << "failed to load bmp surface from " << filename;
        exit(1);
    }
        
    sheetw = bmp->w;
    sheeth = bmp->h;
    buildDefaultFramelist(framew, frameh, rowsize);
    load(bmp, usekey, colourkey);
}

void CSpriteSheet::loadSheet(SDL_Surface *surface, bool usekey, unsigned int colourkey, int framew, int frameh, int rowsize) {
    sheetw = surface->w;
    sheeth = surface->h;
    buildDefaultFramelist(framew, frameh, rowsize);
    load(surface, usekey, colourkey);
    
    VIDEODBG(
        cout << "SpriteSheet stats:" << endl;
        cout << "sheetw = " << sheetw << " sheeth = " << sheeth << endl;
        cout << "num actions = " << numactions << " num frames = " << numFrames(0) << endl;
    )
}

SpriteFrame *CSpriteSheet::getFrame() const {
    return framelist->frames;
}

SpriteFrame *CSpriteSheet::getFrame(int frame) const {
    if (frame < framelist->numframes) {
        return &framelist->frames[frame];
    }
}

SpriteFrame *CSpriteSheet::getFrame(int action, int frame) const {
    if (action < numactions) {
        if (frame < framelist[action].numframes) {
            return &framelist[action].frames[frame];
        }
    }
}

SpriteFrame *CSpriteSheet::getFrame(int action, int frame, int direction) const {
    if (action < numactions) {
        int ndirs = framelist[action].numdirections;
        if (frame < framelist[action].numframes) {
            // You're allowed to specify an invalid direction, this frame may only have one direction
            if (direction >= ndirs)
                direction = 0;
            return &framelist[action].frames[frame * ndirs + direction];
        }
    }
}

int CSpriteSheet::numFrames(int action) {
    if (action < numactions) {
        return framelist[action].numframes;
    }
}

int CSpriteSheet::numDirections(int action) {
    if (action < numactions) {
        return framelist[action].numdirections;
    }
}

/*
SDL_Rect CSpriteSheet_BASE::getFrameRect(int frame) const {
    SDL_Rect srect;

    srect.x = (rowsize ? framew * (frame % rowsize) : framew * frame);
    srect.y = (rowsize ? frameh * (frame / rowsize) : 0);
    srect.w = framew;
    srect.h = frameh;
    //srect.x = 0;
    //srect.y = 0;
    //srect.w = 0;
    //srect.h = 0;
    
    return srect;
}

SDL_Rect CSpriteSheet_BASE::getFrameRect(SpriteFrame *frame) const {
    return *(SDL_Rect *)frame;
}
*/

/* For reading in a file
CSpriteSheet_BASE::framelisting () {
    
    // make the list
    numactions = number actions
    framelist = new ActionFrameList[numactions];
    
    for (int i = 0; i < numactions; ++i) {
        // make an action
        framelist[i].numframes = ;
        framelist[i].numdirections = ;
        SpriteSheetFrame *ptr = (framelist[i].frames = new SpriteSheetFrame[<frames> * <directions>]);
        for 

}
*/

void CSpriteSheet::buildDefaultFramelist(int framew, int frameh, int rowsize) {
    numactions = 1;
    framelist = new SpriteFrameList[1];
    framelist->numframes = (rowsize && frameh ? sheeth / frameh : 1) * (framew ? (rowsize ? rowsize : sheetw / framew) : 1);
    framelist->numdirections = 1;
    SpriteFrame *ptr = (framelist->frames = new SpriteFrame[framelist->numframes]);
    for (int i = 0; i < (rowsize && frameh ? sheeth / frameh : 1); ++i) {
        for (int j = 0; j < (framew ? (rowsize ? rowsize : sheetw / framew) : 1); ++j, ++ptr) {
            ptr->x = j * framew;
            ptr->y = i * frameh;
            ptr->w = framew;
            ptr->h = frameh;
            ptr->shiftx = 0;
            ptr->shifty = 0;
        }
    }
}

void CSpriteSheet::buildDefaultFramelist() {
    numactions = 1;
    framelist = new SpriteFrameList[1];
    framelist->numframes = 1;
    framelist->numdirections = 1;
    SpriteFrame *ptr = (framelist->frames = new SpriteFrame[1]);
    ptr->x = 0;
    ptr->y = 0;
    ptr->w = sheetw;
    ptr->h = sheeth;
    ptr->shiftx = 0;
    ptr->shifty = 0;
}
