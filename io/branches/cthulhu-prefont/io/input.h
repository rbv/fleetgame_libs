#ifndef INPUT_H
#define INPUT_H

#include <deque>
#include <algorithm>

//#include "text.h"  // for CTextField

using namespace std;

/*  Interface classes
 *
 * A class which listens to mouse or keyboard input implements these interfaces
 * Class methods return a bool - if true, the event is passed on to the next
 * class in CInput's list of listeners
 */

class CKeyInputHandler {
    public:
        virtual bool addChar(char) = 0;
        //virtual bool backspace() = 0;
};

class CMouseInputHandler {
    public:
        //virtual void leftClick(int x, int y) = 0;  // use leftRelease() instead
        /* called ONLY if not released in the same tick */
        virtual bool leftPress(int x, int y) = 0;
        /* use as if "leftClick", and also to signal the end of a drag
         * should not require a leftPress event before release 
         */
        virtual bool leftRelease(int x, int y) = 0;
        /* called every tick button is being kept down and mouse moves
         * initial mouse position at start of drag, and current position
         */
        virtual bool leftDrag(int ix, int iy, int fx, int fy) = 0;
        
        /* right now doesn't seem like they'll be a need for lots
         * of right mouse button handling
        */
        virtual bool rightRelease(int x, int y) = 0;
};

/*
class CSystemEventHandler {
    public:
        // window closed (this seems a bit pointless)
        virtual bool quit() = 0;
};
*/

template<class HandlerT>
class CInputDispatcher {
    protected:
        deque<HandlerT *> handlers;
        
        template<typename T>
        void dispatch(bool (HandlerT::*method)(T), T datum) {
            typename deque<HandlerT *>::iterator ptr = handlers.begin();
            while (ptr != handlers.end() && ((*ptr)->*method)(datum))
                ptr++;
        }
        
        template<typename T1, typename T2>
        void dispatch(bool (HandlerT::*method)(T1, T2), T1 datum1, T2 datum2) {
            typename deque<HandlerT *>::iterator ptr = handlers.begin();
            while (ptr != handlers.end() && ((*ptr)->*method)(datum1, datum2))
                ptr++;
        }

        template<typename T1, typename T2, typename T3, typename T4>
        void dispatch(bool (HandlerT::*method)(T1, T2, T3, T4), T1 datum1, T2 datum2, T3 datum3, T4 datum4) {
            typename deque<HandlerT *>::iterator ptr = handlers.begin();
            while (ptr != handlers.end() && ((*ptr)->*method)(datum1, datum2, datum3, datum4))
                ptr++;
        }
        
    public:        
        // add an input handler
        void addHandler(HandlerT *handler, bool tofront = false) {
            if (tofront == false)
                handlers.push_back(handler);
            else if (tofront == true)
                handlers.push_front(handler);
        }
        
        // replace a handler with a new one
        void replaceHandler(HandlerT *old, HandlerT *repl) {
            typename deque<HandlerT *>::iterator pos = find(handlers.begin(), handlers.end(), old);
            if (pos != handlers.end())
                *pos = repl;
        }
        
        // scrub a handler from the event listening queue
        void removeHandler(HandlerT *handler) {
            handlers.erase(find(handlers.begin(), handlers.end(), handler));
        }
};
        

/* This class handles all the user input and calls the correct handlers,
 * etc.
 */
class CInput : public CInputDispatcher<CKeyInputHandler>, CInputDispatcher<CMouseInputHandler> {
    private:
        using CInputDispatcher<CKeyInputHandler>::dispatch;
        using CInputDispatcher<CMouseInputHandler>::dispatch;

        int drag_ix, drag_iy;

    public:
        CInput();
        
        // called once every tick from the master loop
        int pollInput();
        
        using CInputDispatcher<CKeyInputHandler>::addHandler;
        using CInputDispatcher<CMouseInputHandler>::addHandler;
        
        using CInputDispatcher<CKeyInputHandler>::replaceHandler;
        using CInputDispatcher<CMouseInputHandler>::replaceHandler;
        
        using CInputDispatcher<CKeyInputHandler>::removeHandler;
        using CInputDispatcher<CMouseInputHandler>::removeHandler;
           
        unsigned char *keystate;
};

#ifdef TEXT_H

/* An inputfield is a CTextField which sets itself as a keyboard input
 * handler and lets the player type in text. 
*/
class CInputField : public CTextField, public CKeyInputHandler {
    private:
        //static void inputHandler(char letter);
        //static void deleteHandler();
        bool active;
        int minlen;
    public:
        CInputField() { minlen = 0; }  // change?
        CInputField(int fieldwidth, int fieldheight = -1);
        CInputField(const string &copy, int fieldwidth, int fieldheight = -1);
        
        // hook and unhook key input
        void activate();
        void deactivate();
        
        // grab the text the user entered
        const char *getInputStr();
        
        
        ///// CKeyInputHandler methods
        bool addChar(char ch);
        

};

#endif

extern CInput *input;

#endif
