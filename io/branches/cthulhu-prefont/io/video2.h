#ifndef VIDEO_H
#define VIDEO_H

#define VIDEODBG(A) ;
//#define VIDEODBG(A) A

#include <iostream>

#include <SDL/SDL.h>
#include <GL/gl.h>
//#include <GL/glu.h>

#include "rect.h"

struct SDLSpriteData {
    SDL_Surface *surface;
};

struct OGLSpriteData {
	GLuint texref;
    bool blend;
    int texh, texw;
};

//class CSpriteSheet;
class CVideo;

class CSprite {
    friend class CVideo;
    protected:
        typedef void (CSprite::*loadmethod_t)(SDL_Surface *, bool, unsigned int, bool);
        typedef void (CSprite::*drawmethod_t)(SDL_Rect);
        union {
            SDLSpriteData sdl;
            OGLSpriteData ogl;
        };
		static loadmethod_t loadmethod;
		static drawmethod_t drawmethod;
		
		void create() {}
		void destroy() {}
		
	public:
		CSprite();
		~CSprite();
		
		/* load a sprite. The surface provided will be freed */
		void load(SDL_Surface *surface, bool usekey = 0, unsigned int colourkey = 0, bool keepsurface = 0) {
			(this->*loadmethod)(surface, usekey, colourkey, keepsurface);
		}
			
		void draw(SDL_Rect where) {
			(this->*drawmethod)(where);
		}

		/* Split a Sprite up into num tiles, creating a SpriteSheet and destroying the Sprite */
		//CSpriteSheet *split(SDL_Rect &tilesize, int num);

};

// Support struct for CSpriteSheet

struct SpriteFrame {
    int x, y;
    int w, h;
    //int action, direction, frame;
    int shiftx, shifty;
    
    //RectBlock *rect() { return (RectBlock *)this; }
    //RectBlock *rect() { return reinterpret_cast<RectBlock *>(this); }
};

/* The following 2 classes hold all the backend specific code for CSprite and CSpriteSheet
 * Not polymorphic
 * some methods pointer to with method pointers */

class CSDLSprite : private CSprite {
    friend class CVideo;
    public:
        /* Constructor, destructor */
        void create();
        void destroy();  
        /* pointed to by drawmethod and loadmethod in CSprite */
        void loadSprite(SDL_Surface *surface, bool usekey, unsigned int colourkey, bool keepsurface);
        void drawSprite(SDL_Rect where);
        
        /* Called by all drawFrames. Supply source rect on sheet, and dest on screen
         */
        void drawRect(SpriteFrame &src, SDL_Rect dest);
};

class COGLSprite : private CSprite {
    friend class CVideo;
	public:
        /* Constructor, destructor */
        void create();
        void destroy();
        
        void loadSprite(SDL_Surface *surface, bool usekey, unsigned int colourkey, bool keepsurface = 0);
        void drawSprite(SDL_Rect where);
        /* Called by all drawFrames. Supply source rect on sheet, and dest on screen
         */
        void drawRect(SpriteFrame &src, SDL_Rect dest);
        
        void calcTexSize(int w, int h);
  
        /* Expects a surface of the right dimensions in 24-bit BMP format
         * Converts to correct format and loads, with alpha as required
         */
        void surfaceToTexture(SDL_Surface *bmp, bool usealpha, unsigned int colourkey);
        
        /* What the hell is this for again? 
         * "is this required? probably should be using an SDL convert function"
         */
        static void invert(unsigned char *ptr, int len);
        
        /* create texture out of pixel data */
        void createTexture(unsigned char *pixels, GLenum format, GLenum type, GLint components);
};


/* This is the set of all SpriteFrames for a single action of a CSpriteSheet
 * Holds numframes * numdirections frames.
 */
struct SpriteFrameList {
    int numframes, numdirections;
    // array of frames which is organised as 2D array: frames[frameno*numdirs + direction]
    SpriteFrame *frames;
};

class CSpriteSheet : public CSprite {
    friend class CVideo;
    public:
        typedef void (CSpriteSheet::*drawmethod_t)(SpriteFrame &, SDL_Rect);
    protected:
        static drawmethod_t drawmethod;
        
        int sheetw, sheeth;
        int numactions;
        // array of numactions number of SpriteFrameLists
        SpriteFrameList *framelist;
        
        void drawRect(SpriteFrame &src, SDL_Rect dest) {
            (this->*drawmethod)(src, dest);
        }
                      
        /* functions to build framelists when none read from file
         * this method takes the same arguments crappy loadSheets do
         * rowsize is number of frames on a row, or 0 if only 1 row
         */
        void buildDefaultFramelist(int framew, int frameh, int rowsize);
        // this one is for a single frame sheet (uses sheetw and sheeth)
        void buildDefaultFramelist();
    public:
        CSpriteSheet();
        //CSpriteSheet(unsigned char *source, int bufsize, int framew, int frameh, int rowsize = 0);
        ~CSpriteSheet();
        
        /* Temporary load methods (until custom file format) */

        void loadSheet(unsigned char *source, bool usekey, unsigned int colourkey, int bufsize, int framew, int frameh, int rowsize = 0);
        void loadSheet(const char *filename, bool usekey, unsigned int colourkey, int framew, int frameh, int rowsize = 0);
        void loadSheet(SDL_Surface *surface, bool usekey, unsigned int colourkey, int framew, int frameh, int rowsize = 0);
        
        /* Permanently supported methods (currently from generated surfaces only) */
        
        // single frame sheet
        void loadSheet(SDL_Surface *surface, bool usekey = 0, unsigned int colourkey = 0, bool keepsurface = 0);

        /* The sheet might have just one frame, just an array of frames
         * (action 0, frame x, direction 0), action and frame (action x, 
         * frame y, direction 0) or all 3 .
         * These are here for functions that need the size of an object's sprite
         */
        SpriteFrame *getFrame() const;
        SpriteFrame *getFrame(int frame) const;
        SpriteFrame *getFrame(int action, int frame) const;
        SpriteFrame *getFrame(int action, int frame, int direction) const;
        
        /* Wrappers for drawRect so that you can specify frame however you wish
         * where is rect on screen to draw to  
         */
        void drawFrame(SDL_Rect where) {
            drawRect(*getFrame(), where);
        }
        void drawFrame(int frame, SDL_Rect where) {
            drawRect(*getFrame(frame), where);
        }
        void drawFrame(int action, int frame, SDL_Rect where) {
            drawRect(*getFrame(action, frame), where);
        }
        void drawFrame(int action, int frame, int direction, SDL_Rect where) {
            drawRect(*getFrame(action, frame, direction), where);
        }
        
        // Number of actions, and number of frames + directions an action has
        int numActions() { return numactions; }
        int numFrames(int action);
        int numDirections(int action);
};


/* set up window, etc
 */
class CVideo {
    friend class CSDLSprite;
    friend class COGLSprite;
    friend class CSprite;
    public:
        enum backend_t {NOINIT, SDL, OPENGL};
    //protected:
		backend_t backend;
        int width, height;
        bool fullscr;
        char windowname[60];
        SDL_Surface *screen;
        
        /* set up all function pointers */
        void setBackend(backend_t type);

    public:
        // these actually start and shut down SDL
        CVideo();
        // just calls close()
        ~CVideo();
        
        void close();

        void switchType(backend_t type);

        //void reapply();  // call to change video settings after create'd
        //bool supported(); // returns true if the settings are supported
        void setSize(int w, int h);
        void setName(char *wname) { strncpy(windowname, wname, 60); }
        int getWidth() { return width; }
        int getHeight() { return height; }
        //void setFull(bool);
        bool fullscreen() { return fullscr; }

        /* debug functions */
        void printInfo();
        

        /* Call this after all options for the window/screen have been set
         * Prehaps have a method to read defaults from somewhere
         */
        virtual void create() {}   // creates the window, applying options

        virtual void updateScreen() {}
        virtual void updateRects(int numrects, SDL_Rect *rects) {}
        virtual void clearScreen(SDL_Color &color) {}
	    
        /* drawing functions */
        virtual void drawBox(RectPoints &box, SDL_Color &colour) {}
};

class CSDLVideo : public CVideo {
    public:
        CSDLVideo() { setBackend(SDL); }
        CSDLVideo(const CVideo &copy) : CVideo(copy) {}
        void create();
        void updateScreen();
        void updateRects(int numrects, SDL_Rect *rects);
        void clearScreen(SDL_Color &color);
        void drawBox(RectPoints &box, SDL_Color &colour);
};

class COGLVideo : public CVideo {
    public:
        COGLVideo() { setBackend(OPENGL); }
        COGLVideo(const CVideo &copy) : CVideo(copy) {}
        void create();
        void updateScreen();
        void updateRects(int numrects, SDL_Rect *rects);
        void clearScreen(SDL_Color &color);
        void drawBox(RectPoints &box, SDL_Color &colour);
};




extern CVideo *video;


#endif
