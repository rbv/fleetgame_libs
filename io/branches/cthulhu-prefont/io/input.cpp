#include <iostream>

#include <SDL/SDL.h>  // for event polling

#include "input.h"
//#include "text.h"

using namespace std;

CInput *input;

////////////////////// CInput methods //////////////////////

CInput::CInput() {
    cout << "Loading input handler" << endl;
    
    SDL_EnableUNICODE(1);
    SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
}

/* Updates input
 * return true to cause game to quit
*/
int CInput::pollInput() {
    SDL_Event event;
    int numkeys;
    
    // poll all the mouse events, THEN call the handlers
    // don't bother with much right click handling yet
    bool leftdown = false, leftup = false, leftdragged = false;
    bool rightup = false;
    int clikx, cliky, dragx, dragy;
    
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_ESCAPE)
                    // this should prehaps be handled by a handler
                    return true;
                else {
                    int key = event.key.keysym.unicode;
                    if (key && key < 256)
                        dispatch(&CKeyInputHandler::addChar, (char)key);
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    leftdown = true;
                    drag_ix = clikx = event.button.x;
                    drag_iy = cliky = event.button.y;
                }
                break;
            case SDL_MOUSEBUTTONUP:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    leftup = true;
                    clikx = event.button.x;
                    cliky = event.button.y;
                }
                if (event.button.button == SDL_BUTTON_RIGHT) {
                    rightup = true;
                    clikx = event.button.x;
                    cliky = event.button.y;
                }
                break;
            case SDL_MOUSEMOTION:
                if (SDL_BUTTON(SDL_BUTTON_LEFT) & event.motion.state) {
                    leftdragged = true;
                    dragx = event.motion.x;
                    dragy = event.motion.y;
                }
                break;
            case SDL_QUIT:
                return true;
        }
    }
    
    if (leftdown && !leftup)
        dispatch(&CMouseInputHandler::leftPress, clikx, cliky);
        
    if (leftup)
        dispatch(&CMouseInputHandler::leftRelease, clikx, cliky);
        
    if (leftdragged && !leftup)
        dispatch(&CMouseInputHandler::leftDrag, drag_ix, drag_iy, dragx, dragy);
        
    if (rightup)
        dispatch(&CMouseInputHandler::rightRelease, clikx, cliky);


    keystate = SDL_GetKeyState(&numkeys);
        
    return false;
}

////////////////////// CInputField methods //////////////////////
#ifdef TEXT_H

CInputField::CInputField(int fieldwidth, int fieldheight) {
    this->fieldwidth = fieldwidth;
    this->fieldheight = fieldheight;
    minlen = 0;
}

CInputField::CInputField(const string &copy, int fieldwidth, int fieldheight)
            : CTextField(copy, fieldwidth, fieldheight) {
    minlen = str.length();
}
/*
void CInputField::inputHandler(char ch) {
    if (ch == '\n')
        input->deactivate();
    else {
        input->str += ch;
        input->render();
    }
}
void CInputField::deleteHandler() {
    if (str.size())
        str.resize(str.size() - 1);	
}
*/
bool CInputField::addChar(char ch) {
    //cout << "handled in " << ch << " " << (int)ch << " " << (int)'\n' << endl;
    if (ch == '\n' || ch == '\r')
        deactivate();
    else if (ch == '\b') {
        if (str.size() > minlen) {
            str.resize(str.size() - 1);	
            render();
        }
    } else {
        //cout << "before: \"" << str;
        str += ch;
        //cout << "\" after: \"" << str << "\"" << endl;
        render();
    }
    return false;
}


/* Are these sort of redundant? */
void CInputField::activate() {
    input->setTextHandler(this);
}

void CInputField::deactivate() {
    input->removeTextHandler(this); 
}

const char *CInputField::getInputStr() {
    return str.c_str() + minlen;
}

#endif
