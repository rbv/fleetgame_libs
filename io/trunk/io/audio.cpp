#include <iostream>
#include <cstdlib> 



#include "audio.h" 

using namespace std; 

CAudio::CAudio() {
    cout << "Loading audio module" << endl;

      spec.freq = 11025;

      spec.format = AUDIO_U8;

      spec.channels = 1;

      spec.samples = 4096;

      spec.callback = audioCallback;
      spec.userdata = NULL;

      //if (SDL_OpenAudio(&spec, NULL)) {
            
      if (Mix_OpenAudio(44100, AUDIO_U16SYS, 2, 4096)) {
            cerr << "Open Audio system failed." << endl;

            exit(-1);
      }

      //SDL_PauseAudio(0);

      audio_buf = NULL;
      buflen = 0;
      bufoffset = 0;
      
      // REAL code
      
      music1 = 0;

} 

CAudio::~CAudio() {
    cout << "Shutting down audio system" << endl;
    
    //if (audio_buf)
    //    SDL_FreeWAV(audio_buf);
    //endMusic();
    //SDL_CloseAudio();
    Mix_CloseAudio();
    if (music1)
        Mix_FreeMusic(music1);
    while (sounds.size()) {
        Mix_FreeChunk(sounds.back());
        sounds.pop_back();
    }
} 

void CAudio::playMusic(const char *file) {
    /*
    if (!audio_buf) {
        //currently can only play one sound at once
        
        if (SDL_LoadWAV(file, &extraspec, &audio_buf, &buflen) == NULL) {
            cerr << "Loading " << file << " failed." << endl;
            exit(-1);
        }
        bufoffset = 0;
        SDL_PauseAudio(0);
        cout << "Loaded file " << file << endl;
        cout << "Buffer length: " << buflen << endl;
        
    }
    */
    //mychunk = Mix_LoadWAV(file);
    music1 = Mix_LoadMUS(file);
    cerr << "Error: " << Mix_GetError() << endl;
    //if (Mix_PlayChannel (-1, mychunk, 1) == -1) {
    if (Mix_PlayMusic(music1, -1) == -1) {
        cerr << "Crap, file " << file << " did not play" << endl;
        cerr << "Error: " << Mix_GetError() << endl;
        exit(1);
    }
} 

void CAudio::playSound(const char *file) {
    Mix_Chunk *temp = Mix_LoadWAV(file);
    if (temp) {
        if (Mix_PlayChannel (-1, temp, 1) == -1) {
            cerr << "Crap, file " << file << " did not play" << endl;
            cerr << "Error: " << Mix_GetError() << endl;
            //exit(1);
            Mix_FreeChunk(temp);
        } else
            sounds.push_back(temp);
    }
}

void CAudio::stopMusic() {
    /*
    SDL_FreeWAV(audio_buf);
    audio_buf = NULL;
    buflen = 0;
    bufoffset = 0;
    */
    //SDL_PauseAudio(1);
}

// UNUSED since adoption of SDL_Mixer

void audioCallback(void *userdata, Uint8 *stream, int len) {
    cout << "Audio callback: buf len is " << len << endl;
    memcpy(stream, audio->audio_buf + audio->bufoffset, len < audio->buflen ? len : audio->buflen);
    if ((audio->buflen -= len) < 0) {
        audio->stopMusic();
    }
    audio->bufoffset += len;
    cout << "Offset is " << audio->bufoffset << ", sample buffer is " << audio->buflen << endl;
}
