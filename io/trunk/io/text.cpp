#include <iostream>
#include <vector>
#include <sstream>
#include <string.h>
#include <ctype.h>

#include "text.h"

using namespace std;


CTextEngine *text;

//////////////////// CFont methods ////////////////////

//CFont::CFont(const char *file, int pt) {
CFont::CFont(font_t fonttype, const char *file, int pt) {
    type = fonttype;
    ptr = NULL;
    ownership = true;
    transfer = true;
    /*
    if (strlen(file) > 5) {
        char buf[5];
        for (char *ptr = strcpy(buf, file + strlen(file) - 4); *ptr != 0; ptr++)
            *ptr = tolower(*ptr);
        if (!strcmp(buf, ".ttf")) {
            cout << "TTF: " << buf << endl;
            type = TTF;
        }
        if (!strcmp(buf, ".bmp" {
            cout << "BMP" << buf << endl;
            type = BITMAP;
        }
    }
    */
    if (type == TTF) {
        ttf = TTF_OpenFont(file, pt);
        if (!ttf)
            cerr << "Could not load font from " << file << " : " << TTF_GetError() << endl;
        return;
    }
    /*
    if (type == BITMAP) {
        sheet = new CSpriteSheet;
        // TODO: where am I meant to get the transparency info from?
        sheet->loadSheet(SDL_LoadBMP(file), true, 0xff00ff, 8, 12, 16);
        // Only fixedwidth currently supported
        fixedwidth = true;
        return;
    }
    */
    if (type == BITMAP) {
        surface = SDL_LoadBMP(file);
        if (!surface) {
            cerr << "Could not load font from " << file << " : " << SDL_GetError() << endl;
            return;
        }
        SDL_SetColorKey(surface, SDL_SRCCOLORKEY | SDL_RLEACCEL, SDL_MapRGB(surface->format, 255, 0, 255));
        // Only fixedwidth currently supported
        fixedwidth = true;
        return;
    }
    cerr << "failed to load font from " << file << endl;
    type = NONE;
}

CFont::CFont(const CFont &rhs) : type(rhs.type), ptr(rhs.ptr), fixedwidth(rhs.fixedwidth), ownership(false), transfer(false) {
    if (rhs.type != NONE && rhs.transfer) {
        ownership = rhs.ownership;
        const_cast<CFont &>(rhs).ownership = false;
    }
}

CFont &CFont::operator=(const CFont &rhs) {
    destroy();
    //return *this = CFont(rhs);
    type = rhs.type;
    ptr = rhs.ptr;
    fixedwidth = rhs.fixedwidth;
    ownership = false;
    transfer = false;
    return *this;
}

void CFont::destroy() {
    if (ownership && !ptr) {
        if (type == TTF)
            TTF_CloseFont(ttf);
        else
            SDL_FreeSurface(surface);
    }
}

//////////////////// CTTF_TextEngine methods ////////////////////

CTextEngine::CTextEngine() {
    cout << "Loading text engine" << endl;
    
    if (TTF_Init() == -1) {
        cerr << "Could not initialise SDL_TTF. Error: " << TTF_GetError();
        exit(-1);
    }
    defaultfont = new CFont(CFont::TTF, "airstrip.ttf", 12);
    //defaultfont = new CFont(CFont::BITMAP, "curses_640x300.bmp");
    if (!defaultfont->ptr) {
        // loading failed, so delete the useless font obj
        delete defaultfont;
        defaultfont = NULL;
    }
}

CTextEngine::~CTextEngine() {
    cout << "Shutting down text engine" << endl;
    if (defaultfont)
        delete defaultfont;
    TTF_Quit();
}

/*
SDL_Surface *CTTF_TextEngine::printText(const char *text) {
    SDL_Color col = {255, 255, 255, 0};
    return TTF_RenderText_Solid(deffont, text, col);
}
*/

//////////////////// CTTFTextEngine methods ////////////////////

SDL_Surface *CTTFTextEngine::renderString(CFont &font, const char *text, SDL_Color col) {
    return TTF_RenderText_Solid(font.ttf, text, col);
}

int CTTFTextEngine::fontLineHeight(CFont &font) {
    return TTF_FontLineSkip(font.ttf);
}
    
int CTTFTextEngine::fontCharWidth(CFont &font, const char ch) {
    int ad;
    TTF_GlyphMetrics(font.ttf, ch, NULL, NULL, NULL, NULL, &ad);
    return ad;
}

int CTTFTextEngine::fontStringWidth(CFont &font, const char *str) {
    int w;
    TTF_SizeText(font.ttf, str, &w, NULL);
    return w;
}

//////////////////// CBitmapTextEngine methods ////////////////////

/*  These are methods which take sheet as a CSpriteSheet pointer
SDL_Surface *CBitmapTextEngine::renderString(CFont &font, const char *text, SDL_Color col) {
    SpriteFrame *frames = font.sheet->getFrame(0);
    while (!*text) {
        
}

int CBitmapTextEngine::fontLineHeight(CFont &font) {
    return font.sheet->getFrame(0)->h;
}

int CBitmapTextEngine::fontCharWidth(CFont &font, const char ch) {
    return font.sheet->getFrame(ch)->w;
}

int CBitmapTextEngine::fontStringWidth(CFont &font, const char *str) {
    if (font.fixedwidth)
        return strlen(str) * font.sheet->getFrame(0)->w;
    else {
        int length = 0;
        SpriteFrame *frames = font.sheet->getFrame(0);
        while (!*str)
            length += frames[*str++].w;
        return length;
    }
}
*/

SDL_Surface *CBitmapTextEngine::renderString(CFont &font, const char *text, SDL_Color col) {
    SDL_Surface *rendered;
    SDL_Rect srect, drect;
    srect.w = fontCharWidth(font, 0);
    srect.h = fontLineHeight(font);
    drect.x = drect.y = 0;
    rendered = SDL_CreateRGBSurface(SDL_SWSURFACE, fontStringWidth(font, text), fontLineHeight(font), 8, 0, 0, 0, 0);
    SDL_SetColorKey(rendered, SDL_SRCCOLORKEY, 0);
    rendered->format->palette->colors[1] = col;
    while (!*text) {
        srect.x = srect.w * (*text % 16);
        srect.y = srect.h * (*text / 16);
        SDL_BlitSurface(font.surface, &srect, rendered, &drect);
        drect.x += srect.w;
        text++;
    }
    return rendered;
}

int CBitmapTextEngine::fontLineHeight(CFont &font) {
    if (font.fixedwidth)
        return font.surface->h / 16;
}

int CBitmapTextEngine::fontCharWidth(CFont &font, const char ch) {
    if (font.fixedwidth)
        return font.surface->w / 16;
}

int CBitmapTextEngine::fontStringWidth(CFont &font, const char *str) {
    if (font.fixedwidth)
        return strlen(str) * (font.surface->w / 16);
}

//////////////////// CTextField methods ////////////////////

CTextField::CTextField() : inserter(str) {
    dontrender = false;
    surface = NULL;
    sprite = NULL;
    color.r = 255;
    color.g = 255;
    color.b = 255;
    font = text->deffont();
    fieldheight = -1;
    fieldwidth = video->getWidth();
}

CTextField::CTextField(const string &str, int fieldwidth, int fieldheight) : 
            str(str), fieldwidth(fieldwidth), fieldheight(fieldheight), inserter(str) {
    dontrender = false;
    surface = NULL;
    sprite = NULL;
    color.r = 255;
    color.g = 255;
    color.b = 255;
    font = text->deffont();
    render();
}

CTextField::CTextField(const char *str, const SDL_Color &color, int fieldwidth, int fieldheight) : 
            str(str), fieldwidth(fieldwidth), fieldheight(fieldheight), inserter(str) {
    dontrender = false;
    surface = NULL;
    sprite = NULL;
    this->color = color;
    font = text->deffont();
    render();
}

CTextField::CTextField(const CFont &font, const SDL_Color &color, int fieldwidth, int fieldheight) : 
            font(font), color(color), fieldwidth(fieldwidth), fieldheight(fieldheight), inserter(str) {
    dontrender = false;
    surface = NULL;
    sprite = NULL;
}

CTextField::~CTextField() {
    wipe();
}

void CTextField::wipe() {
    /*if (sheet) {
        delete sheet;
        sheet = NULL;
    }*/ /*else {
        // <s>sheet given pointer to surface. It assumes control of it and will delete it itself</s>
        if (surface)
            SDL_FreeSurface(surface);
        surface = NULL;
    }*/
    if (surface) {
        SDL_FreeSurface(surface);
        surface = NULL;
    }
    if (sprite) {
        delete sprite;
        sprite = NULL;
    }
}

CSprite &CTextField::getSprite() {
    if (!sprite) {
        sprite = new CSprite();
        SDL_Color &col = surface->format->palette->colors[0];
        sprite->load(surface, true, SDL_MapRGB(surface->format, col.r, col.g, col.b), true);
    }
    return *sprite;
}

void CTextField::render() {
    if (font.type == CFont::TTF)
        _render<CTTFTextEngine>();
    if (font.type == CFont::BITMAP)
        _render<CBitmapTextEngine>();
}

//template<> void CTextField::_render<CTTFTextEngine>();
//template<> void CTextField::_render<CBitmapTextEngine>();

template<class renderer>
void CTextField::_render() {
    int w, ad, lineskip = renderer::fontLineHeight(font);
    // start is pointer to the part of the string still to be to processed (split)
    char fragment[100];  
    const char *start, *find;
    int i = 0, total, lines = 1; // i is offset from start
    vector<SDL_Surface *> r_lines;  // rendered lines
    SDL_Rect dest;  // for blitting each line to the final surface
    SDL_Surface *temp;

    wipe();
        
    TXTDBG(cout << "rendering text: \"" << str << "\" with fieldwith " << fieldwidth << " and lineskip " << lineskip << endl;)
    
    start = str.c_str();
    for (;;) {
        TXTDBG(cout << "==loop for line" << lines << " text is: " << start << endl;)
        //TTF_SizeText(font, start, &w, &h);
        w = renderer::fontStringWidth(font, start);
        TXTDBG(cout << "width is " << w << endl;)
        // by default assume chopping has to occur
        if (w <= fieldwidth) {
            // no chopping needed
            //temp = TTF_RenderText_Solid(font, start, color);
            temp = renderer::renderString(font, start, color);
            if (temp)
                r_lines.push_back(temp);
            break;
        }
        total = 0;
        while (total < fieldwidth) {
            //TTF_GlyphMetrics(font, start[i], NULL, NULL, NULL, NULL, &ad);
            total += renderer::fontCharWidth(font, start[i]);
            i++;
        }
        TXTDBG(cout << "i is " << i << endl;)
        
        // find the last space before cutoff point
        find = start + i;
        while (*find != ' ' && find > start + i / 2) // the idea being that it doesn't put 1 letter on a line?
            find--;
        if (find != start + i / 2) {
            i = find - start;
            i++; // skip the space
        }
        TXTDBG(cout << "new i is " << i << endl;)
        
        /*  //  string object-based code I didn't use
        split = s.substr(start, i).s.rfind(' ');
        if (split != string::npos)
            i = split;
        */
        
        strncpy(fragment, start, i);
        fragment[i] = 0; //unneeded?
        if (fieldheight != -1 && (lines + 1) * lineskip > fieldheight) {
            // last line
            strcpy(fragment + i - 1, "...");
            //temp = TTF_RenderText_Solid(font, fragment, color);
            temp = renderer::renderString(font, fragment, color);
            if (temp)
                r_lines.push_back(temp);
            break;
        }
        lines++;
    
        start = &start[i];
        i = 0;
        //temp = TTF_RenderText_Solid(font, fragment, color);
        temp = renderer::renderString(font, fragment, color);
        if (temp)
            r_lines.push_back(temp);
    }
    
    // Now patch all the lines together! :(
    TXTDBG(cout << "total lines: " << lines << "vector size: " << r_lines.size() << endl;)
    
    // total is width of the new surface
    for (i = 0, total = 0; i < r_lines.size(); i++) {
        //cout << (int)r_lines[i] << endl;
        if (r_lines[i]->w > total) 
            total = r_lines[i]->w;
    }
    TXTDBG(cout << "total is: " << total << flush<< endl;)
    // I don't know what the last 4 0's here are for
    surface = SDL_CreateRGBSurface(SDL_SWSURFACE, total, lines * lineskip + 1, 8, 0, 0, 0, 0);
    SDL_SetColorKey(surface, SDL_SRCCOLORKEY, 0);
    
    //SDL_Color blk = {0, 0, 0, 0};
    //for(int i = 0; i < 256; i++) { surface->format->palette->colors[i] = blk; }
    surface->format->palette->colors[1] = color;
    
    dest.x = 0;
    dest.y = r_lines.size() * lineskip;
    for (i = r_lines.size() - 1; i >= 0; i--) {
        temp = r_lines.back();
        dest.y -= lineskip;
        SDL_BlitSurface(temp, NULL, surface, &dest);
        SDL_FreeSurface(temp);
        r_lines.pop_back();
    }
    

    TXTDBG(cout << surface->format->palette->colors[1].r << " " << surface->format->palette->colors[1].g << surface->format->palette->colors[1].b << endl;)
    
    //sheet = new CSpriteSheet((unsigned char *)surface->pixels, surface->w * surface->h, surface->w, surface->h);
/*
    sheet = new CSpriteSheet;
    sheet->loadSheet(surface, true, 0, surface->w, surface->h);
*/
    //sheet->loadSheet(surface, false, 0, surface->w, surface->h);
}

void CTextField::changeStr(const string &str) {
    this->str = str;
    if (!dontrender)
        render();
}

void CTextField::changeStr(const char *str) {
    this->str = str;
    if (!dontrender)
        render();
}

void CTextField::changeStr(int number) {
    stringstream temp;
    temp << number;
    this->str = temp.str();
    if (!dontrender)
        render();
}

void CTextField::setFont(CFont &f) {
    font = f;
    if (!dontrender)
        render();
}

void CTextField::setColor(SDL_Color &color) {
    this->color = color;
    if (surface)
        surface->format->palette->colors[1] = color;
}

/*
void CTextField::draw(SDL_Rect &where) {
    if (sheet)
        sheet->drawFrame(where);
}
*/

CTextField &CTextField::clear() {
    str.clear();
    if (!dontrender)
        render();
    return *this;
}

void CTextField::pause() {
    dontrender = true;   
}

void CTextField::resume() {
    dontrender = false;
    render();
}

/* this is for debug */
/*
void CTextField::strout() {
    cout << endl << "string length:" << str.length() << endl;
    for (int i = str.length(); i > 0; i--) {
        int ad, mx, w;
        cout << "char " << i << " is " << str[i - 1] << " width ";
        TTF_GlyphMetrics(font, str[i - 1], NULL, &mx, NULL, NULL, &ad);
        cout << ad << " (with maxx " << mx << ") text length is ";
        TTF_SizeText(font, str.c_str(), &w, NULL);
        cout << w << endl;
        str[i-1] = 0;
    }
}
*/



