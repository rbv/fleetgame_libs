#ifndef GENERAL_H
#define GENERAL_H

#ifdef __GNUC__
#define pure_function __attribute__((const))
#else
#define pure_function
#endif

#define STARTUP if(SDL_Init(0)) {\
                 cerr << "SDL failed to init!" << SDL_GetError();\
                 exit(-1);}\
                 atexit(SDL_Quit);

#endif
