#ifndef 
#define
/* A buffer for use to speed up rendering of (backdrops mainly) which consist of lots
 * of tiny tiles which don't change often
 * Unfortunately I haven't observed much speed up with the SDL graphics backend using this,
 * only OGL
 */


class CRenderedBuffer {
    private:
        int sizex, sizey;
        int borderx, bordery;
        
    public:
        
        ~CRenderedBuffer();

        /* Call before and after drawing graphics to the screen normally
         * Overwrites current buffer contents and resets
         */
        void draw_Start();
        void draw_End();
        
        /* Call when moving the camera, before redrawing the uncovered pieces
         * retu
         */
        void moveView(int x, int y);
    
};

#endif
