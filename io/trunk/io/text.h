#ifndef TEXT_H
#define TEXT_H


#if 0
#define TXTDBG(A) A
#else
#define TXTDBG(A) ;
#endif

#include <string>
#include <iostream>
#include <sstream>
//#include <hash_map>

#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

#include "video2.h"
using namespace std;

class CFont {
    friend class CTextEngine;
    friend class CTTFTextEngine;
    friend class CBitmapTextEngine;
    friend class CTextField;
    public:
        enum font_t {NONE, BITMAP, TTF};
    private:
        font_t type;
        union {
//            CSpriteSheet *sheet;
            SDL_Surface *surface;
            TTF_Font *ttf;
            void *ptr;
        };
        bool fixedwidth;   // Bitmapped only
        bool ownership;    // responsible for the allocated object?
        bool transfer;     // pass ownership when copying? (on by default)
        
        void destroy();
    public:
        CFont() : ptr(NULL), type(NONE), ownership(false), transfer(true) {}
        CFont(font_t fonttype, const char *file, int pt = 12);
        CFont(const CFont &rhs); // creates a CFont object to which ownership is transferred
        ~CFont() { destroy(); }
        
        CFont &operator=(const CFont &rhs); // ownership is kept by the old object
        
        void holdFast() { transfer = false; }
};


/* The plan right now is that this class keeps track of...
 * actually, lets wait for the gui class. maybe that should
 * keep track of all the text onscreen
 */
class CTextEngine {
    friend class CTextField;
    private:
        /*
        struct FontKey {
            char filename[14];
            int pt;
        };
        
        struct FontStruct {
            union {
                TTF_Font *ttf;
                CSpriteSheet *
            int pt;
        };
        
        map<TTFFontKey, TTF_Font *> ttf_fonts;
        map<BitmapFontKey, CSpriteSheet *> bitmap_fonts;
        
        void loadBitmapFont(BitmapFontKey &key) {
            BitmapFontKey key;
            for (char *i = filename; i < filename + 14; ++i)
                *i = 0;
                
            
            
            pair<iterator, bool> insert(const value_type& v); 
        }
        */
        CFont *defaultfont;
        
    public:       
        CTextEngine();
        ~CTextEngine();
        
        const CFont &deffont() const { return *defaultfont; }
};

/* used by CTextField::render()
 */
class CTTFTextEngine {
    public:
        static SDL_Surface *renderString(CFont &font, const char *text, SDL_Color col);
        static int fontLineHeight(CFont &font);
        static int fontCharWidth(CFont &font, const char ch);
        static int fontStringWidth(CFont &font, const char *str);
};

/* used by CTextField::render()
 */
class CBitmapTextEngine {
    public:        
        static SDL_Surface *renderString(CFont &font, const char *text, SDL_Color col);
        static int fontLineHeight(CFont &font);
        static int fontCharWidth(CFont &font, const char ch);
        static int fontStringWidth(CFont &font, const char *str);
};

/* text renderer using SDL_ttf, which uses FreeType
 * automatically wraps lines, manages field size, displays text
 */
//template<class TextEngine, TextEngine *renderer>
class CTextField {
    protected:
        string str;
        ostringstream inserter;
        SDL_Color color;
        SDL_Surface *surface;  // sheet is actually given this surface, 
        CSprite *sprite;  // only created (out of surface) if requested
	    CFont font;
        bool dontrender;  
        int fieldwidth;
        int fieldheight;
        
        // delete rendered
        void wipe();
        
        // wrapper for function below
        void render();
        
        template<class renderer>
        void _render();
    public:
        CTextField();
        CTextField(const string &str, int fieldwidth, int fieldheight = -1);
        CTextField(const char *str, const SDL_Color &color, int fieldwidth, int fieldheight = -1);
        CTextField(const CFont &font, const SDL_Color &color, int fieldwidth, int fieldheight = -1);
        ~CTextField();
        
        SDL_Surface *getSurface() { return surface; }  // kept for purposes other than drawing (?)
        CSprite &getSprite();
        
        void changeStr(const string &str);
        void changeStr(const char *str);
        /* shortcut functions! quick debug! */
        void changeStr(int number);
         
        const string &getStr() { return str; }
        
        void setFont(CFont &f);
        void setColor(SDL_Color &color);
        
        /* clear string and re-render (why?) */
        CTextField &clear();
        
        /* Pause and resume rendering, when changing string frequently but don't
         * want to blit it immediately
         */
        void pause();
        void resume();
        
        // WHAT! this is useless!
        template<typename T>
        CTextField &operator <<(T data) {
            inserter.str(str);
            inserter << data;
            str = inserter.str();
            if (!dontrender)
                render();
            return *this;
        }
        
        CTextField &operator =(const char *text) {
            changeStr(text);
            return *this;
        }
        
        int getHeight() { return surface ? surface->h : 0; }
        int getWidth() { return surface ? surface->w : 0; }
        
        //void strout();  //debug purposes
};


extern CTextEngine *text;

#endif
