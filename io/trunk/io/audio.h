#ifndef AUDIO_H

#define AUDIO_H 

#include <vector>

#include <SDL/SDL.h> 
#include <SDL/SDL_mixer.h>

using namespace std; 

class CAudio {
    friend void audioCallback(void *userdata, Uint8 *stream, int len);

    private:

        SDL_AudioSpec spec, extraspec;

        unsigned char *audio_buf;

        unsigned int buflen;
        unsigned int bufoffset;
        //Mix_Chunk *mychunk;
        vector<Mix_Chunk *> sounds;
        Mix_Music *music1, *music2;  // 2nd is for fading between musics

    public:

        CAudio();
        ~CAudio(); 

        void playSound(const char *file);
        //void playSound(const char *file, float volume);
        void stopMusic();
            
        void playMusic(const char *file);
        //void fadeToMusic(const char *file);

}; 

void audioCallback(void *userdata, Uint8 *stream, int len); 

extern CAudio *audio; 

#endif
