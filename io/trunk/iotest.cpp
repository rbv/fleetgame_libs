/* Standard includes */
/*
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
//#include <stdio.h>
#include <string>
*/
#include <iostream>

/* Library includes */
#include <SDL/SDL.h>
#include <GL/gl.h>

/* Program includes */
#include "io/general.h"
#include "io/video2.h"
#include "io/input.h"
#include "io/text.h"
//#include "io/audio.h"

using namespace std;


//CVideo *video;

/*
int main(int argc, char *argv[]) {  
    if (SDL_Init(SDL_INIT_VIDEO)) {
        cerr << "SDL failed to init!";
        exit(-1);
    }
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_Surface *screen = SDL_SetVideoMode(200, 200, 32, SDL_OPENGL);
    if (!screen) {
        cerr << "SetVideoMode failed!";
        exit(-1);
    }
    SDL_WM_SetCaption("OPENGL", NULL);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    SDL_GL_SwapBuffers();
    int stime = SDL_GetTicks();
    
    while(SDL_GetTicks() < stime + 4000) SDL_Delay(15);
    
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
    
    SDL_InitSubSystem(SDL_INIT_VIDEO);
    
    screen = SDL_SetVideoMode(200, 200, 32, 0);
    if (!screen)
    {
        cerr << "SetVideoMode failed!";
        exit(-1);
    }
    SDL_WM_SetCaption("SDL", NULL);
    SDL_FillRect(screen, NULL, 0xffffffff);
    SDL_Flip(screen);
    stime = SDL_GetTicks();
    
    while(SDL_GetTicks() < stime + 4000) SDL_Delay(15);
    
    SDL_Quit();
}
*/

class KeyListener : public CKeyInputHandler {
    public:
        bool addChar(char in) {
            cout << "Hey! You pressed " << in << "!" << endl;
            return true;
        }
};

class SuperKeyListener : public CKeyInputHandler {
    public:
        bool addChar(char in) {
            if (in == 'f') {
                cout << "Filtering out f key" << endl;
                return false;
            }
            return true;
        }
};

void drawloop() {
    int stime;
    SDL_Rect rect = {300, 200, 0, 0}, rect2 = {450, 200, 0, 0}, textrect = {100, 100, 0, 0};
    SDL_Color red = {255, 50, 50, 0};
    
    
    CSprite nymph;
    CSprite rock;
    CSpriteSheet ascii;
//    CTextField line(text->deffont(), red, 400);
//    line.changeStr("Hello World!");
    CTextField line;
    line.changeStr("Hello World!");
    
    nymph.load(SDL_LoadBMP("nymph.bmp"));
    rock.load(SDL_LoadBMP("rock24.bmp"), true, 0xffffff);      
    ascii.loadSheet(SDL_LoadBMP("curses_640x300.bmp"), true, 0xff00ff, 8, 12, 16);

    for(stime = SDL_GetTicks(); SDL_GetTicks() < stime + 5000; SDL_Delay(15)) {
        nymph.draw(rect);
        rock.draw(rect2);
        line.getSprite().draw(textrect);
        /*
        textrect.x = 100;
        textrect.y = 100;
        for(char *ptr = "Hello World!"; *ptr != 0; ++ptr) {
            textrect.w = 0;
            textrect.h = 0;
            ascii.drawFrame(*ptr, textrect);
            textrect.x += ascii.getFrame(*ptr)->w;
        }
        */
        
        
        if (input->pollInput())
            break;
        video->updateScreen();
    }
}

int main(int argc, char *argv[]) {
    cout.setf(ios::unitbuf);
    STARTUP

    video = new COGLVideo;
    video->create();
    text = new CTextEngine;
    input = new CInput;
    
    KeyListener listendevice;
    input->addHandler(&listendevice);
    SuperKeyListener higherdevice;
    input->addHandler(&higherdevice, true);

    drawloop();
    
    delete text;
    video->switchType(CVideo::OPENGL);
    video->create();
    text = new CTextEngine;
    
    drawloop();
    
    delete input;
    delete text;
    delete video;
    return 0;
}
